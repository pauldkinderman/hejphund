package com.citi.hejphund.utilities;

import org.apache.http.client.fluent.Request;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.logging.Logger;

@Component
public class CompanyNameGrabber {
    private static CompanyNameGrabber ourInstance = new CompanyNameGrabber();

    public static CompanyNameGrabber getInstance() {
        return ourInstance;
    }

    private String baseAPIurl;

    private CompanyNameGrabber() {
        baseAPIurl = "http://nyc31.conygre.com:31/Stock/getCompanyNameForSymbol/";
    }

    public String getCompanyName(String ticker){
        String url = baseAPIurl.concat(ticker);
        String response = null;
        try {
            response = Request.Get(url)
                    .connectTimeout(1000)
                    .socketTimeout(1000)
                    .execute().returnContent().asString();
            //Get rid of the quote around the response string
            response = response.substring(1, response.length()-1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
