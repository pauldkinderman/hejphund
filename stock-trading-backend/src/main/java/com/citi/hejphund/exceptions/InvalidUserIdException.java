package com.citi.hejphund.exceptions;

public class InvalidUserIdException extends Exception{
    public InvalidUserIdException() {
    }

    public InvalidUserIdException(String message) {
        super(message);
    }

    public InvalidUserIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidUserIdException(Throwable cause) {
        super(cause);
    }
}
