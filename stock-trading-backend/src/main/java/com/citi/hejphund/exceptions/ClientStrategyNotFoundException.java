package com.citi.hejphund.exceptions;

public class ClientStrategyNotFoundException extends Exception {

    public ClientStrategyNotFoundException() {
        super();
    }

    public ClientStrategyNotFoundException(String message) {
        super(message);
    }

    public ClientStrategyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientStrategyNotFoundException(Throwable cause) {
        super(cause);
    }
}
