package com.citi.hejphund.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum TradingStrategy implements Serializable {
    TWO_MOVING_AVERAGES("Two Moving Averages"),
    BOLLINGER_BANDS("Bollinger Bands"),
    PRICE_BREAKOUT("Price Breakout");

    private String stringRepresentation;

    TradingStrategy(String stringRepresentation) {
        this.stringRepresentation = stringRepresentation;
    }

    @JsonValue
    public String getStringRepresentation() {
        return stringRepresentation;
    }

    public void setStringRepresentation(String stringRepresentation) {
        this.stringRepresentation = stringRepresentation;
    }
}
