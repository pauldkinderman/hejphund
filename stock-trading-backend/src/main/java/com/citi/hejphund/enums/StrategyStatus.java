package com.citi.hejphund.enums;

import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum StrategyStatus implements Serializable {
    RUNNING("Running"),
    OPENED("Opened"),
    EXITED("Exited");

    private String stringRepresentation;

    StrategyStatus(String stringRepresentation) {
        this.stringRepresentation = stringRepresentation;
    }

    @JsonValue
    public String getStringRepresentation() {
        return stringRepresentation;
    }

    public void setStringRepresentation(String stringRepresentation) {
        this.stringRepresentation = stringRepresentation;
    }
}