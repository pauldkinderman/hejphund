package com.citi.hejphund.data;

import com.citi.hejphund.entities.StockPNLSummary;
import com.citi.hejphund.entities.Strategy;
import com.citi.hejphund.entities.StrategySummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StrategyRepository extends JpaRepository<Strategy,Integer> {

    @Query(nativeQuery = true)
    Iterable<StockPNLSummary> getStockPNLSummary(int userId);

    @Query(nativeQuery = true)
    Iterable<StrategySummary> getStrategySummary(int userId, String tickerSymbol);

}
