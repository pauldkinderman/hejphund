package com.citi.hejphund.data;

import com.citi.hejphund.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
    Iterable<Transaction> findTransactionsByStrategyId(int strategyId);

    Iterable<Transaction> findTransactionsByStockTickerSymbol(String stockTickerSymbol);
}
