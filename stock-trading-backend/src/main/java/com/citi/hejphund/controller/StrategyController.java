package com.citi.hejphund.controller;

import com.citi.hejphund.entities.StockPNLSummary;
import com.citi.hejphund.entities.Strategy;
import com.citi.hejphund.entities.StrategyStatusUpdatePacket;
import com.citi.hejphund.entities.StrategySummary;
import com.citi.hejphund.exceptions.ClientStrategyNotFoundException;
import com.citi.hejphund.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/hejphund/")
@CrossOrigin
public class StrategyController {

    Logger logger = Logger.getLogger(StrategyController.class.getName());

    @Autowired
    private ClientService clientService;
    public StrategyController(ClientService clientService) {

        this.clientService = clientService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "strategies/{userId}")
    public Iterable<StockPNLSummary> getStockPNLSummary(@PathVariable("userId") int userId){
        return clientService.getStockPNLSummary(userId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "strategies/{userId}/{companySymbol}")
    public Iterable<StrategySummary> getStrategySummary(@PathVariable("userId") int userId,
                                                        @PathVariable("companySymbol") String companySymbol){
        return clientService.getStrategySummary(userId, companySymbol);
    }

    @RequestMapping(method = RequestMethod.POST, value = "strategies")
    public @ResponseBody
    Strategy addNewStrategy(@RequestBody Strategy strategy){
        return clientService.placeStrategy(strategy);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "strategies/status")
    public Strategy updateStrategyStatus(@RequestBody StrategyStatusUpdatePacket packet){
        return clientService.updateStrategyStatus(packet);
    }

    @ExceptionHandler(ClientStrategyNotFoundException.class)
    public Strategy strategyNotFoundExceptionHandler(ClientStrategyNotFoundException e) {
        logger.warning("The strategy queried is not found");
        return new Strategy();
    }

}
