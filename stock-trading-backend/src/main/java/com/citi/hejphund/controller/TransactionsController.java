package com.citi.hejphund.controller;

import com.citi.hejphund.entities.Transaction;
import com.citi.hejphund.exceptions.ClientStrategyNotFoundException;
import com.citi.hejphund.exceptions.InvalidUserIdException;
import com.citi.hejphund.exceptions.TransactionNotFoundException;
import com.citi.hejphund.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sun.misc.Request;

import java.util.logging.Logger;

@RestController
@RequestMapping("/api/hejphund/transactions")
@CrossOrigin
public class TransactionsController {

    Logger logger = Logger.getLogger(Transactional.class.getName());

    @Autowired
    TransactionService transactionService;

    @RequestMapping(method=RequestMethod.GET, value="/{userId}/strategy/{strategyId}")
    public Iterable<Transaction> getTransactionForStrategy(@PathVariable("userId") int userId,
                                                           @PathVariable int strategyId)
    throws InvalidUserIdException {
        return transactionService.getTransactionsForOneStrategy(userId, strategyId);
    }

    @RequestMapping(method=RequestMethod.GET, value="/{userId}")
    public Iterable<Transaction> getTransactionForUser(@PathVariable("userId") int userId)
            throws InvalidUserIdException {
        return transactionService.getAllTransactionForUser(userId);
    }

    @RequestMapping(method=RequestMethod.GET, value="/{userId}/company/{ticker}")
    public Iterable<Transaction> getTransactionOfTickerForUser(@PathVariable("userId") int userId,
                                                               @PathVariable("ticker") String stockSymbol)
            throws InvalidUserIdException {
        return transactionService.getAllTransactionForTicker(userId, stockSymbol);
    }

    @ExceptionHandler(InvalidUserIdException.class)
    public Transaction invalidUserIdExceptionHandler(InvalidUserIdException e){
        logger.warning(e.getMessage());
        return new Transaction();
    }

}