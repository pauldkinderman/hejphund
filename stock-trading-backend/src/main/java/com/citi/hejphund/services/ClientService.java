package com.citi.hejphund.services;

import com.citi.hejphund.data.StrategyRepository;
import com.citi.hejphund.entities.*;
import com.citi.hejphund.enums.StrategyStatus;
import com.citi.hejphund.exceptions.ClientStrategyNotFoundException;
import com.citi.hejphund.utilities.CompanyNameGrabber;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private StrategyRepository dao;
    @Autowired
    private CompanyNameGrabber companyNameGrabber;

    public ClientService(StrategyRepository dao, CompanyNameGrabber nameGrabber) {
        this.dao = dao;
        this.companyNameGrabber = nameGrabber;
    }

    public void assignPortfolio(Portfolio portfolio) {
    }

    public Strategy placeStrategy(Strategy strategy) {
        return dao.save(generateFieldsForNewStrategy(strategy));
    }
    private Strategy generateFieldsForNewStrategy(Strategy strategy){
        strategy.setCompanyName(companyNameGrabber.getCompanyName(strategy.getStockTickerSymbol()));
        strategy.setDollarPNL(0.0);
        strategy.setPercentPNL(0.00);
        strategy.setInitialValue(0.0);
        strategy.setStrategyStatus(StrategyStatus.OPENED);
        return strategy;
    }

    public Strategy updateStrategyStatus(StrategyStatusUpdatePacket statusUpdate) {
        Strategy strategy = dao.findById(statusUpdate.getId()).get();
        return dao.save(setStrategyStatus(statusUpdate.getStatus(), strategy));
    }
    private Strategy setStrategyStatus(String statusUpdate, Strategy strategy){
        if(strategy.getStrategyStatus() == StrategyStatus.EXITED || strategy.getStrategyStatus() == StrategyStatus.OPENED){
            return strategy;
        }
        else if (statusUpdate.equals("Exit")) {
            strategy.setStrategyStatus(StrategyStatus.EXITED);
        }
        else { }
        return strategy;
    }

    public Iterable<StrategySummary> getStrategySummary(int userId, String stockSymbol){
        return dao.getStrategySummary(userId, stockSymbol);
    }

    public Iterable<StockPNLSummary> getStockPNLSummary(int userId){
        return dao.getStockPNLSummary(userId);
    }

    public Strategy getStrategyById(int orderId) throws ClientStrategyNotFoundException {
        Optional<Strategy> orderOptional =  dao.findById(orderId);
        if (orderOptional.isPresent()){
            return orderOptional.get();
        } else {
            throw new ClientStrategyNotFoundException();
        }
    }
}

