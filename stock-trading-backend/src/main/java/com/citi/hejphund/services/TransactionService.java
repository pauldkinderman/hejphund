package com.citi.hejphund.services;

import com.citi.hejphund.data.TransactionRepository;
import com.citi.hejphund.entities.Transaction;
import com.citi.hejphund.exceptions.InvalidUserIdException;
import com.citi.hejphund.exceptions.TransactionNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepository dao;

    public TransactionService(TransactionRepository dao) {
        this.dao = dao;
    }


    public Iterable<Transaction> getTransactionsForOneStrategy(int userId, int strategyId)
            throws InvalidUserIdException{
        if(userId != 1){
            throw new InvalidUserIdException("UserId specified for the strategy does not match");
        }
        return dao.findTransactionsByStrategyId(strategyId);
    }

    public Iterable<Transaction> getAllTransactionForUser(int userId) throws InvalidUserIdException {
        if(userId != 1){
            throw new InvalidUserIdException("UserId specified is invalid");
        }
        return dao.findAll();
    }

    public Iterable<Transaction> getAllTransactionForTicker(int userId, String stockTicker) throws InvalidUserIdException {
        if(userId !=1){
            throw new InvalidUserIdException("UserId specified is invalid");
        }
        return dao.findTransactionsByStockTickerSymbol(stockTicker);
    }

}
