package com.citi.hejphund.entities;

import com.citi.hejphund.enums.StrategyStatus;
import com.citi.hejphund.enums.TradingStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@SqlResultSetMappings({

        @SqlResultSetMapping(
            name="StockAndStrategyCountMapping",
            classes = {
                @ConstructorResult(
                    targetClass = StockPNLSummary.class,
                    columns = {
                        @ColumnResult(name = "stockSymbol", type = String.class),
                        @ColumnResult(name = "strategyCount", type = Integer.class)
                    }
                )
            }
        ),

        @SqlResultSetMapping(
            name="StrategySummaryMapping",
            classes = {
                @ConstructorResult(
                    targetClass = StrategySummary.class,
                    columns = {
                            @ColumnResult(name = "id", type = Integer.class),
                        @ColumnResult(name = "strategyAlias", type = String.class),
                        @ColumnResult(name = "strategyType", type = String.class),
                        @ColumnResult(name = "sharesAffected", type = Integer.class),
                        @ColumnResult(name = "percentPNL", type = Double.class),
                        @ColumnResult(name = "dollarPNL", type = Double.class),
                            @ColumnResult(name = "initialValue", type = Double.class),
                    }
                )
            }
        ),

        @SqlResultSetMapping(
                name = "StockPNLSummaryMapping",
                classes = {
                    @ConstructorResult(
                            targetClass = StockPNLSummary.class,
                            columns = {
                                    @ColumnResult(name = "stockCompany", type = String.class),
                                    @ColumnResult(name = "stockSymbol", type = String.class),
                                    @ColumnResult(name = "netInitialValue", type = Double.class),
                                    @ColumnResult(name = "netPercentPNL", type = Double.class),
                                    @ColumnResult(name = "netDollarPNL", type = Double.class),
                                    @ColumnResult(name = "netShare", type = Integer.class),
                                    @ColumnResult(name = "strategyCount", type = Integer.class),
                            }
                    )
                }
        )
})

@NamedNativeQueries({
        @NamedNativeQuery(
                name="Strategy.getEachStockAndStrategyCount",
                query = "select s.stock_symbol as stockSymbol, count(s.strategy_type) as strategyCount " +
                        "from strategies as s group by s.stock_symbol",
                resultSetMapping = "StockAndStrategyCountMapping"
        ),

        @NamedNativeQuery(
                name="Strategy.getStrategySummary",
                query = "select " +
                        "id, " +
                        "strategy_alias as strategyAlias, " +
                        "initial_value as initialValue, " +
                        "strategy_type as strategyType, " +
                        "total_transaction_volume as sharesAffected, " +
                        "percent_PNL as percentPNL, " +
                        "dollar_PNL as dollarPNL " +
                        "from strategies where client_id = ?1 and stock_symbol = ?2",
                resultSetMapping = "StrategySummaryMapping"
        ),

        @NamedNativeQuery(
                name = "Strategy.getStockPNLSummary",
                query = "select " +
                        "stock_company as stockCompany, " +
                        "stock_symbol as stockSymbol, " +
                        "sum(initial_value) as netInitialValue, " +
                        "sum(dollar_PNL)/sum(initial_value) as netPercentPNL, " +
                        "sum(dollar_PNL) as netDollarPNL, " +
                        "sum(total_transaction_volume) as netShare, " +
                        "count(stock_symbol) as strategyCount " +
                        "From strategies where client_id = ?1 group by stock_symbol",
                resultSetMapping = "StockPNLSummaryMapping"
        ),

})

@Entity @Table(name = "strategies")
public class Strategy implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "stock_company") private String companyName;
    @Column(name = "stock_symbol") private String stockTickerSymbol;
    @Column(name = "total_transaction_volume") private Integer volume;
    @Column(name = "client_id") private Integer clientId;
    @Column(name = "strategy_alias") private String strategyAlias;
    @Column(name = "percent_PNL") private Double percentPNL;
    @Column(name = "dollar_PNL") private Double dollarPNL;
    @Column(name = "time_period") private Integer timePeriod;
    @Column(name = "initial_value") private Double initialValue;


    @Column(name = "strategy_type")
    @Enumerated(EnumType.STRING)
    private TradingStrategy strategyType;

    @Column(name = "strategy_status")
    @Enumerated(EnumType.STRING)
    private StrategyStatus strategyStatus;


    public Strategy() {}

    public Strategy(
            Integer clientId,
            String strategyAlias,
            TradingStrategy strategyType,
            String companyName,
            String stockTickerSymbol,
            Integer totalTransVolume,
            StrategyStatus strategyStatus,
            Double percentPNL,
            Double dollarPNL,
            Integer timePeriod,
            Double initialValue)
    {
        this.companyName = companyName;
        this.stockTickerSymbol = stockTickerSymbol;
        this.volume = totalTransVolume;
        this.clientId = clientId;
        this.strategyAlias = strategyAlias;
        this.strategyType = strategyType;
        this.strategyStatus = strategyStatus;
        this.percentPNL = percentPNL;
        this.dollarPNL = dollarPNL;
        this.timePeriod = timePeriod;
        this.initialValue = initialValue;
    }

    public Strategy(
            Integer clientId,
            String strategyAlias,
            TradingStrategy strategyType,
            String companyName,
            String stockTickerSymbol,
            Integer totalTransVolume,
            Integer timePeriod)
    {
        this.companyName = companyName;
        this.stockTickerSymbol = stockTickerSymbol;
        this.volume = totalTransVolume;
        this.clientId = clientId;
        this.strategyAlias = strategyAlias;
        this.strategyType = strategyType;
        this.strategyStatus = StrategyStatus.OPENED;
        this.percentPNL = 0.0;
        this.dollarPNL = 0.0;
        this.timePeriod = timePeriod;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStockTickerSymbol() {
        return stockTickerSymbol;
    }

    public void setStockTickerSymbol(String stockTickerSymbol) {
        this.stockTickerSymbol = stockTickerSymbol;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Integer getClientId() { return clientId; }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getStrategyAlias() {
        return strategyAlias;
    }

    public void setStrategyAlias(String strategyAlias) {
        this.strategyAlias = strategyAlias;
    }

    public TradingStrategy getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(TradingStrategy strategyType) {
        this.strategyType = strategyType;
    }

    public StrategyStatus getStrategyStatus() {
        return strategyStatus;
    }

    public void setStrategyStatus(StrategyStatus strategyStatus) {
        this.strategyStatus = strategyStatus;
    }

    public Double getPercentPNL() {
        return percentPNL;
    }

    public void setPercentPNL(Double percentPNL) {
        this.percentPNL = percentPNL;
    }

    public Double getDollarPNL() {
        return dollarPNL;
    }

    public void setDollarPNL(Double dollarPNL) {
        this.dollarPNL = dollarPNL;
    }

    public Integer getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(Integer timePeriod) {
        this.timePeriod = timePeriod;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Strategy strategy = (Strategy) o;
        return Objects.equals(id, strategy.id) &&
                Objects.equals(companyName, strategy.companyName) &&
                Objects.equals(stockTickerSymbol, strategy.stockTickerSymbol) &&
                Objects.equals(volume, strategy.volume) &&
                Objects.equals(clientId, strategy.clientId) &&
                Objects.equals(strategyAlias, strategy.strategyAlias) &&
                Objects.equals(percentPNL, strategy.percentPNL) &&
                Objects.equals(dollarPNL, strategy.dollarPNL) &&
                strategyType == strategy.strategyType &&
                strategyStatus == strategy.strategyStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, companyName, stockTickerSymbol, volume, clientId, strategyAlias, percentPNL, dollarPNL, strategyType, strategyStatus);
    }
}
