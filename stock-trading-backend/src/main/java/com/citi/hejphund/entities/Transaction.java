package com.citi.hejphund.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity @Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int tid;


    @Column(name="stock_symbol") private String stockTickerSymbol;
    @Column(name="buy_value") private Double buyPrice;
    @Column(name="sell_value") private Double sellPrice;
    @Column(name="volume") private Integer volume;
    @Column(name="trans_datetime") private LocalDateTime datetTime;

    @JoinColumn (name="sid", referencedColumnName="id", nullable = false)
    @ManyToOne
    @JsonIgnore
    private Strategy strategy;

    public Transaction() {}

    public Transaction(String stockTickerSymbol,
                       Double buyPrice,
                       Double sellPrice,
                       Integer volume,
                       Strategy strategy,
                       LocalDateTime dateTime) {
        this.stockTickerSymbol = stockTickerSymbol;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.volume = volume;
        this.strategy = strategy;
        this.datetTime = dateTime;
    }

    public LocalDateTime getDatetTime() {
        return datetTime;
    }

    public void setDatetTime(LocalDateTime datetTime) {
        this.datetTime = datetTime;
    }

    public int getTid() {
        return tid;
    }

    public void setTid(int tid) {
        this.tid = tid;
    }

    public String getStockTickerSymbol() {
        return stockTickerSymbol;
    }

    public void setStockTickerSymbol(String stockTickerSymbol) {
        this.stockTickerSymbol = stockTickerSymbol;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return getTid() == that.getTid() &&
                getStockTickerSymbol().equals(that.getStockTickerSymbol()) &&
                getBuyPrice().equals(that.getBuyPrice()) &&
                getSellPrice().equals(that.getSellPrice()) &&
                getVolume().equals(that.getVolume()) &&
                getStrategy().equals(that.getStrategy()) &&
                getDatetTime().equals(that.getDatetTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTid(), getStockTickerSymbol(), getBuyPrice(), getSellPrice(), getVolume(), getStrategy(), getDatetTime());
    }
}
