package com.citi.hejphund.entities;

import java.io.Serializable;

public class StrategyStatusUpdatePacket implements Serializable {
    private Integer id;
    private String status;

    public StrategyStatusUpdatePacket(Integer id, String statusUpdate) {
        this.id = id;
        this.status = statusUpdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
