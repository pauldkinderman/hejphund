package com.citi.hejphund.entities;

import java.io.Serializable;

public class StockPNLSummary implements Serializable {

    private String stockCompany;
    private String stockSymbol;
    private Double netInitialValue;
    private Double netPercentPNL;
    private Double netDollarPNL;
    private Integer netShare;
    private Integer strategyCount;

    public StockPNLSummary() {
    }

    public StockPNLSummary(String stockCompany,
                           String stockSymbol,
                           Double netInitialValue,
                           Double netPercentPNL,
                           Double netDollarPNL,
                           Integer netShare,
                           Integer strategyCounts)
    {
        this.stockCompany = stockCompany;
        this.stockSymbol = stockSymbol;
        this.netInitialValue = netInitialValue;
        this.netPercentPNL = netPercentPNL;
        this.netDollarPNL = netDollarPNL;
        this.netShare = netShare;
        this.strategyCount = strategyCount;
    }

    public String getStockCompany() {
        return stockCompany;
    }

    public void setStockCompany(String stockCompany) {
        this.stockCompany = stockCompany;
    }

    public String getStockSymbol() {
        return stockSymbol;
    }

    public void setStockSymbol(String stockSymbol) {
        this.stockSymbol = stockSymbol;
    }

    public Double getNetInitialValue() {
        return netInitialValue;
    }

    public void setNetInitialValue(Double netInitialValue) {
        this.netInitialValue = netInitialValue;
    }

    public Double getNetPercentPNL() {
        return netPercentPNL;
    }

    public void setNetPercentPNL(Double netPercentPNL) {
        this.netPercentPNL = netPercentPNL;
    }

    public Double getNetDollarPNL() {
        return netDollarPNL;
    }

    public void setNetDollarPNL(Double netDollarPNL) {
        this.netDollarPNL = netDollarPNL;
    }

    public Integer getNetShare() {
        return netShare;
    }

    public void setNetShare(Integer netShare) {
        this.netShare = netShare;
    }

    public Integer getStrategyCount() {
        return strategyCount;
    }

    public void setStrategyCount(Integer strategyCount) {
        this.strategyCount = strategyCount;
    }

}
