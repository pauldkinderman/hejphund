package com.citi.hejphund.entities;

import com.citi.hejphund.enums.TradingStrategy;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public class StrategySummary implements Serializable {

    private Integer id;
    private String strategyName;
    private String strategyType;
    private Integer sharesAffected;
    private Double percentPNL;
    private Double dollarAmountPNL;
    private Double initialValue;
    public StrategySummary(){ }

    public StrategySummary(
                            Integer id,
                           String strategyName,
                           String strategyType,
                           Integer sharesAffected,
                           Double percentPNL,
                           Double dollarPNL,
                            Double initialValue)
    {
        this.id = id;
        this.strategyName = strategyName;
        this.strategyType = strategyType;
        this.sharesAffected = sharesAffected;
        this.percentPNL = percentPNL;
        this.dollarAmountPNL = dollarPNL;
        this.initialValue = initialValue;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public String getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(String strategyType) {
        this.strategyType = strategyType;
    }

    public int getSharesAffected() {
        return sharesAffected;
    }

    public void setSharesAffected(int sharesAffected) {
        this.sharesAffected = sharesAffected;
    }

    public double getPercentPNL() {
        return percentPNL;
    }

    public void setPercentPNL(double percentPNL) {
        this.percentPNL = percentPNL;
    }

    public double getDollarAmountPNL() {
        return dollarAmountPNL;
    }

    public void setDollarAmountPNL(double dollarAmountPNL) {
        this.dollarAmountPNL = dollarAmountPNL;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }
}
