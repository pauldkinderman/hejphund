package com.citi.hejphund.repo;

import com.citi.hejphund.data.StrategyRepository;
import com.citi.hejphund.entities.Strategy;
import com.citi.hejphund.enums.StrategyStatus;
import com.citi.hejphund.enums.TradingStrategy;
import com.citi.hejphund.services.ClientService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration
@DataJpaTest // use an in memory database
@SpringBootTest(classes={com.citi.hejphund.AppConfig.class})
@TestPropertySource(locations = "classpath:application-test.properties") // this is only needed because swagger breaks tests
public class TestStrategyRepository {
    @Autowired
    private TestEntityManager manager;

    @Autowired // this is a mock which is injected because of the @DataJpaTest
    private StrategyRepository repo;

    @Autowired
    ClientService clientService;

    @Test
    public void canAddStrategy(){
        Strategy strategy = new Strategy(
                1,
                "Apple Breakout1",
                TradingStrategy.PRICE_BREAKOUT,
                "Apple Inc",
                "AAPL",
                100,
                StrategyStatus.OPENED,
                5.23,
                1000.0,
                324,
                0.0
        );

        Strategy createdStrategy = manager.persist(strategy);
        assert(strategy.equals(createdStrategy));
    }
    @Test
    public void canGetStrategyById() {
        Strategy strategy =
        new Strategy(
                1,
                "Apple Breakout1",
                TradingStrategy.PRICE_BREAKOUT,
                "Apple Inc",
                "AAPL",
                100,
                StrategyStatus.OPENED,
                5.23,
                1000.0,
                315,
                0.0
        );
        int id = (int)manager.persistAndGetId(strategy);

        Strategy savedStraategy = repo.findById(id).get();
        assert(strategy.equals(savedStraategy));
    }

    @Test
    public void canUpdateStrategy(){
        Strategy strategy = new Strategy(
                1,
                "Apple Breakout1",
                TradingStrategy.PRICE_BREAKOUT,
                "Apple Inc",
                "AAPL",
                100,
                StrategyStatus.OPENED,
                5.23,
                1000.0,
                345,
                0.0
        );
    }
}
