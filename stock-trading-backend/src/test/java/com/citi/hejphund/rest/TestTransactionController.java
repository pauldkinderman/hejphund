package com.citi.hejphund.rest;

import com.citi.hejphund.entities.Strategy;
import com.citi.hejphund.entities.Transaction;
import com.citi.hejphund.services.ClientService;
import com.citi.hejphund.services.TransactionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes={com.citi.hejphund.AppConfig.class})
@TestPropertySource(locations = "classpath:application-test.properties")
public class TestTransactionController {

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private TransactionService mockTransactionService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void canGetTransactionsForOneStrategy() throws Exception{
        Strategy strategy = new Strategy();
        strategy.setId(1);

        Transaction transaction1 = new Transaction(
                "AAPL",
                100.0,
                110.0,
                300,
                strategy,
                LocalDateTime.of(
                        LocalDateTime.now().getYear(),
                        LocalDateTime.now().getMonth(),
                        LocalDateTime.now().getDayOfMonth(),
                        LocalDateTime.now().getHour(),
                        LocalDateTime.now().getMinute(),
                        LocalDateTime.now().getSecond())
        );

        List<Transaction> trancList = new ArrayList<>();

        trancList.add(transaction1);

        given(mockTransactionService.getTransactionsForOneStrategy(1, 1))
                .willReturn(trancList);

        mockMvc.perform(
                get("/api/hejphund/transactions/1/1")
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].stockTickerSymbol", is("AAPL"))
                );

        verify(mockTransactionService).getTransactionsForOneStrategy(1,1);

    }

}
