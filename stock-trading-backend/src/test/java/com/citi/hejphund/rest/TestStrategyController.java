package com.citi.hejphund.rest;

import com.citi.hejphund.entities.StockPNLSummary;
import com.citi.hejphund.entities.Strategy;
import com.citi.hejphund.entities.StrategyStatusUpdatePacket;
import com.citi.hejphund.entities.StrategySummary;
import com.citi.hejphund.enums.StrategyStatus;
import com.citi.hejphund.enums.TradingStrategy;
import com.citi.hejphund.services.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes={com.citi.hejphund.AppConfig.class})
@TestPropertySource(locations = "classpath:application-test.properties")
public class TestStrategyController {

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private ClientService mockClientService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void canAddNewClientOrder() throws Exception {
        Strategy strategy = new Strategy(
                1,
                "Apple Breakout1",
                TradingStrategy.PRICE_BREAKOUT,
                "Apple Inc",
                "AAPL",
                100,
                StrategyStatus.OPENED,
                5.23,
                1000.0,
                600,
                0.0
        );

        given(mockClientService.placeStrategy(strategy)).willReturn(strategy);

        String json = mapper.writeValueAsString(strategy);
        mockMvc.perform(
                    post("/api/hejphund/strategies")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json)
                    .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.stockTickerSymbol", is("AAPL")));
        verify(mockClientService).placeStrategy(any(Strategy.class));

    }

    @Test
    public void canGetStockPNLSummary() throws Exception{
        StockPNLSummary stockPNLSummary1 = new StockPNLSummary(
                "Apple Inc",
                "AAPL",
                0.0,
                8.12,
                100.0,
                10,
                10
        );
        StockPNLSummary stockPNLSummary2 = new StockPNLSummary(
                "Microsoft",
                "MSFT",
                0.0,
                8.12,
                100.0,
                10,
                10
        );

        List<StockPNLSummary> stockPNLSummaries = new ArrayList<>();
        stockPNLSummaries.add(stockPNLSummary1);
        stockPNLSummaries.add(stockPNLSummary2);

        given(mockClientService.getStockPNLSummary(1)).willReturn(stockPNLSummaries);

        mockMvc.perform (
                get("/api/hejphund/strategies/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].stockSymbol", is("AAPL")))
                .andExpect(jsonPath("$[1].stockSymbol", is("MSFT"))
        );

        verify(mockClientService).getStockPNLSummary(1);
    }

    @Test
    public void canGetStrategySummary() throws Exception{
        StrategySummary strategySummary = new StrategySummary(
                1,
                "strategy1",
                "Price Breakout",
                100,
                -2.31,
                111.0,
                55.1
        );

        List<StrategySummary> summaryList = new ArrayList<>();
        summaryList.add(strategySummary);

        given(mockClientService.getStrategySummary(1, "AAPL"))
                .willReturn(summaryList);

        mockMvc.perform (
                get("/api/hejphund/strategies/1/AAPL")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].strategyName", is("strategy1"))
                );

        verify(mockClientService, times(1)).getStrategySummary(1, "AAPL");

    }

    @Test
    public void canUpdateClientOrderStatus() throws Exception {
        Strategy strategy = new Strategy(
                1,
                "Apple Breakout1",
                TradingStrategy.PRICE_BREAKOUT,
                "Apple Inc",
                "AAPL",
                100,
                StrategyStatus.RUNNING,
                5.23,
                1000.0,
                500,
                0.0
        );

        Strategy expectdStrategy = new Strategy(
                1,
                "Apple Breakout1",
                TradingStrategy.PRICE_BREAKOUT,
                "Apple Inc",
                "AAPL",
                100,
                StrategyStatus.EXITED,
                5.23,
                1000.0,
                500,
                0.0
        );


        StrategyStatusUpdatePacket update = new StrategyStatusUpdatePacket(1, "Exit");
        given(mockClientService.updateStrategyStatus(any(StrategyStatusUpdatePacket.class))).willReturn(expectdStrategy);
        String json = mapper.writeValueAsString(update);


        mockMvc.perform (
                    put("/api/hejphund/strategies/status")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json)
                    .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.strategyStatus", is("Exited")));

        verify(mockClientService, times(1)).updateStrategyStatus(any(StrategyStatusUpdatePacket.class));

    }



}

