package com.citi.hejphund.services;

import com.citi.hejphund.data.TransactionRepository;
import com.citi.hejphund.entities.Strategy;
import com.citi.hejphund.entities.Transaction;
import com.citi.hejphund.exceptions.InvalidUserIdException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@RunWith(SpringJUnit4ClassRunner.class)
public class TestTransactionService {

    private TransactionService transactionService;
    private TransactionRepository daoMock;

    @Before
    public void setup(){
        daoMock = mock(TransactionRepository.class);
        transactionService = new TransactionService(daoMock);
    }

    @Test
    public void canGetTransactionForOneStrategy() throws InvalidUserIdException {
        Strategy strategy = new Strategy();
        strategy.setId(1);
        Transaction transaction = new Transaction(
                "AAPL",
                10.21,
                10.41,
                100,
                strategy,
                LocalDateTime.of(
                        LocalDateTime.now().getYear(),
                        LocalDateTime.now().getMonth(),
                        LocalDateTime.now().getDayOfMonth(),
                        LocalDateTime.now().getHour(),
                        LocalDateTime.now().getMinute(),
                        LocalDateTime.now().getSecond())
        );

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        given(daoMock.findTransactionsByStrategyId(1)).willReturn(transactions);

        Iterable<Transaction> returnedList = transactionService.getTransactionsForOneStrategy(1,1);

        assertEquals(returnedList.iterator().next(), transaction);
    }
}
