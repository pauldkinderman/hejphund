package com.citi.hejphund.services;

import com.citi.hejphund.data.StrategyRepository;
import com.citi.hejphund.entities.Strategy;
import com.citi.hejphund.entities.StrategyStatusUpdatePacket;
import com.citi.hejphund.entities.StrategySummary;
import com.citi.hejphund.enums.StrategyStatus;
import com.citi.hejphund.enums.TradingStrategy;

import com.citi.hejphund.utilities.CompanyNameGrabber;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.*;
import static org.assertj.core.api.Assertions.*;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;


public class TestClientService {

    private StrategyRepository daoMock;
    private CompanyNameGrabber nameGrabber;

    private ClientService clientService;

    @Before
    public void setup(){
        daoMock = mock(StrategyRepository.class);
        nameGrabber = CompanyNameGrabber.getInstance();
        clientService = new ClientService(daoMock, nameGrabber);
    }

    @Test
    public void canPlaceStrategy(){
        Strategy strategy = new Strategy(
                1,
                "Strategy",
                TradingStrategy.TWO_MOVING_AVERAGES,
                "",
                "AAPL",
                120,
                null,
                0.1,
                11.0,
                300,
                0.0
        );

        Strategy expectedStrategy = new Strategy(
                1,
                "Strategy",
                TradingStrategy.TWO_MOVING_AVERAGES,
                "Apple Inc",
                "AAPL",
                120,
                StrategyStatus.OPENED,
                0.0,
                0.0,
                300,
                0.0
        );
        System.out.println();
        clientService.placeStrategy(strategy);

        verify(daoMock, times(1)).save(expectedStrategy);



    }

    @Test
    public void canUpdateStrategyStatusToExited() {
        Strategy strategy = new Strategy(
                1,
                "Strategy",
                TradingStrategy.TWO_MOVING_AVERAGES,
                "Apple Inc",
                "AAPL",
                120,
                StrategyStatus.RUNNING,
                0.0,
                0.0,
                215,
                0.0
        );
        Strategy expectedStrategy = new Strategy(
                1,
                "Strategy",
                TradingStrategy.TWO_MOVING_AVERAGES,
                "Apple Inc",
                "AAPL",
                120,
                StrategyStatus.EXITED,
                0.0,
                0.0,
                200,
                0.0
        );
        StrategyStatusUpdatePacket statusUpdatePacket =
                new StrategyStatusUpdatePacket(1, "Exit");

        Optional<Strategy> optionalStrategy = Optional.of(strategy);
        given(daoMock.findById(1)).willReturn(optionalStrategy);

        clientService.updateStrategyStatus(statusUpdatePacket);

        verify(daoMock).save(expectedStrategy);
    }

}
