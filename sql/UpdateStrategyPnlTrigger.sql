
DROP TRIGGER IF EXISTS update_strategy_pnl_from_new_transaction;

DELIMITER //

CREATE TRIGGER update_strategy_pnl_from_new_transaction
	AFTER INSERT ON transactions
	FOR EACH ROW
	BEGIN
    
		UPDATE strategies 
			SET initial_value = IF( initial_value = 0.0, NEW.buy_value*total_transaction_volume, initial_value),
				dollar_PNL = dollar_PNL + NEW.pnl,
				percent_PNL = dollar_PNL/initial_value
		WHERE id=NEW.sid;
	
		
	END//

DELIMITER ;
