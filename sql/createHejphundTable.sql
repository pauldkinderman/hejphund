DROP DATABASE hejphund;

CREATE DATABASE IF NOT EXISTS hejphund;
use hejphund;

DROP TABLE IF EXISTS strategies;
DROP TABLE IF EXISTS transactions;
CREATE TABLE strategies (
	id int primary key auto_increment,
    client_id int not null,
    strategy_alias varchar(100) not null unique,
    strategy_type varchar(50) not null,
    stock_company varchar(50) not null,
    stock_symbol varchar (6) not null,
    total_transaction_volume int not null,
    strategy_status varchar(20) not null,
    percent_PNL double not null,
    dollar_PNL double not null,
    initial_value double,
    time_period int not null
);

CREATE TABLE transactions (
	tid int primary key auto_increment,
	sid int not null,
	stock_symbol varchar(6) not null,
    buy_value double,
    sell_value double,
    volume int not null,
    pnl double not null,
    trans_datetime datetime not null,
	FOREIGN KEY (sid) REFERENCES strategies(id)                    
);

DROP TRIGGER IF EXISTS update_strategy_pnl_from_new_transaction;

insert into strategies values(1, 1, 'Apple MovingAvg1', 'TWO_MOVING_AVERAGES', 'Apple Inc', 'AAPL', 200, 'OPENED', 0.0, 0, 0.0, 400);
insert into strategies values(2, 1, 'Apple Bolinger1', 'TWO_MOVING_AVERAGES', 'Apple Inc', 'AAPL', 200, 'OPENED', 0.55, 72, 0.0, 300);
insert into strategies values(3, 1, 'Apple MovingAvg2', 'TWO_MOVING_AVERAGES', 'Apple Inc', 'AAPL', 200, 'OPENED', 0.62, 102, 0.0, 215);
insert into strategies values(4, 1, 'IBM MovingAvg1', 'TWO_MOVING_AVERAGES', 'IBM', 'IBM', 200, 'OPENED', 0.69, 100, 0.0, 600);
insert into strategies values(5, 1, 'Google Breakout', 'TWO_MOVING_AVERAGES', 'Google', 'GOOG', 200, 'OPENED', 0.60, 100, 0.0, 345);
insert into strategies values(6, 1, 'Intel Bollinger', 'TWO_MOVING_AVERAGES', 'Intel', 'INTC', 200, 'OPENED', 0.60, 100, 0.0, 746);
insert into strategies values(8, 1, 'Amazon Bollinger1', 'TWO_MOVING_AVERAGES', 'Amazon', 'AMZN', 200, 'OPENED', 0.60, 100, 0.0, 415);
insert into strategies values(9, 1, 'Amazon Breakout', 'TWO_MOVING_AVERAGES', 'Amazon', 'AMZN', 200, 'OPENED', 0.60, 100, 0.0, 315);
insert into strategies values(10, 1, 'Amazon Bollinger2', 'TWO_MOVING_AVERAGES', 'Amazon', 'AMZN', 200, 'RUNNING', 0.60, 100, 0.0, 215);
insert into strategies values(11, 1, 'Citi Bollinger', 'TWO_MOVING_AVERAGES', 'Citi', 'C', 200, 'RUNNING', 0.60, 100, 0.0, 115);
insert into strategies values(12, 1, 'Experimenting', 'TWO_MOVING_AVERAGES', 'Apple Inc', 'AAPL', 2,'OPENED', 0.0, 100, 0.0, 300);

insert into transactions values(1, 1, 'AAPL', 180.7, 190.23, 50, 150, now());
insert into transactions values(2, 1, 'AAPL', 190.22, 189.95, 100, 150, now());
insert into transactions values(3, 2, 'MSFT', 190.22, 140.23, 50, 150, now());
insert into transactions values(4, 2, 'MSFT', 190.22, 155.01, 150, 150, now());
insert into transactions values(5, 6, 'INTC', 190.22, 88.11, 120, 150, now());
insert into transactions values(6, 5, 'GOOG', 190.22, 190.23, 50, 150, now());
insert into transactions values(7, 8, 'AMZN', 190.22, 1338.23, 400, 150, now());
insert into transactions values(8, 8, 'AMZN', 190.22, 1338.23, 400, 150, now());
insert into transactions values(9, 3, 'C', 190.22, 70.23, 400, 150, now());

DELIMITER //

CREATE TRIGGER update_strategy_pnl_from_new_transaction
	AFTER INSERT ON transactions
	FOR EACH ROW
	BEGIN
    
		UPDATE strategies 
			SET initial_value = IF( initial_value = 0.0, NEW.buy_value*total_transaction_volume, initial_value),
				dollar_PNL = dollar_PNL + NEW.pnl,
				percent_PNL = dollar_PNL/initial_value
		WHERE id=NEW.sid;
	
		
	END//

DELIMITER ;


