-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: hejphund
-- ------------------------------------------------------
-- Server version	5.7.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `strategies`
--

DROP TABLE IF EXISTS `strategies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `strategies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_symbol` varchar(6) DEFAULT NULL,
  `volume` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `strategy_name` varchar(100) DEFAULT NULL,
  `trading_strategy` varchar(50) DEFAULT NULL,
  `strategy_status` varchar(20) DEFAULT NULL,
  `percent_PNL` double DEFAULT NULL,
  `dollar_PNL` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `strategies`
--

LOCK TABLES `strategies` WRITE;
/*!40000 ALTER TABLE `strategies` DISABLE KEYS */;
INSERT INTO `strategies` VALUES (1,'APPL',200,1,'Apple MovingAvg1','TWO_MOVING_AVERAGES','RUNNING',0.6,100),(2,'APPL',650,1,'Apple Bolinger1','BOLLINGER_BANDS','STOPPED',0.2,100),(3,'APPL',500,1,'Apple MovingAvg2','TWO_MOVING_AVERAGES','RUNNING',0.1,100),(4,'IBM',50,1,'IBM MovingAvg1','TWO_MOVING_AVERAGES','RUNNING',-0.3,100),(5,'GOOG',10,1,'Google Breakout','PRICE_BREAKOUT','STOPPED',-1.6,100),(6,'INT',50,1,'Intel Bollinger','BOLLINGER_BANDS','RUNNING',-9.96,100),(7,'BABA',200,1,'Alibaba Breakout','PRICE_BREAKOUT','STOPPED',3.6,100),(8,'AMZN',400,1,'Amazon Bollinger1','BOLLINGER_BANDS','RUNNING',10.6,100),(9,'AMZN',400,1,'Amazon Breakout','PRICE_BREAKOUT','OPENED',29.6,100),(10,'AMZN',400,1,'Amazon Bollinger2','BOLLINGER_BANDS','OPENED',-8.6,100),(11,'C',400,1,'Citi Bollinger','BOLLINGER_BANDS','OPENED',-3.2,100);
/*!40000 ALTER TABLE `strategies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `strategy_id` int(11) NOT NULL,
  `stock_symbol` varchar(6) DEFAULT NULL,
  `transaction_type` varchar(4) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `volume` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `strategy_id` (`strategy_id`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`strategy_id`) REFERENCES `strategies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,1,'APPL','buy',190.23,50),(2,1,'APPL','buy',189.95,100),(3,2,'MSFT','buy',140.23,50),(4,2,'MSFT','sell',155.01,150),(5,6,'INT','buy',88.11,120),(6,5,'GOOG','buy',190.23,50),(7,8,'AMZN','sell',1338.23,400),(8,8,'AMZN','sell',1338.23,400),(9,3,'C','sell',70.23,400);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-31 18:53:54
