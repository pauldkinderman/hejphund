# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment is

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Paul Kinderman (pk39252)
* Evelyn Galvan (eg46498)
* Hao Sen Zheng (hz57215)
* Jee Graeff (jg42525)

### Docker Image Versions ###
REPOSITORY          TAG      
openjdk             8        
node                10.15.3
mysql               5.7.19
Angular CLI	    8.1.2
