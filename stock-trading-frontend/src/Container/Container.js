import React    from "react";
import clsx from 'clsx';

import CssBaseline from '@material-ui/core/CssBaseline';
import {Drawer} from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import AllStrategies from '../AllStrategies/AllStrategies';
import NavigationBar from '../NavigationBar/NavigationBar';
import CompanyStrategies from "../CompanyStrategies/CompanyStrategies";
import Summary from '../Summary/Summary';
import ApiUtil from "../utility/ApiUtil";

class Container extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openCompany: null,
      open: false,
      strategies: [],
      strategyData: null, 
      strategy: null,
      initialValue: null,
    }
    this.handleStrategySelect.bind(this);
  }
  
  async handleCompanySelect(company) {
    await ApiUtil.getAllStrategiesFromSpecificCompany(company)
    .then(data => {
      this.setState ({
        strategyData: null,
        strategies: data,
        strategy: null,
        initialValue: null,
      });
    }).catch(e => console.log(e));  }

  async handleStrategySelect(strategy, initialValue) {
    this.setState({
      initialValue: initialValue,
      strategy: strategy,
    });
    await ApiUtil.getStrategyDetails(strategy)
    .then(data => {
      if (data.length > 0)
          this.setState({
            strategyData: data
          });
      else 
          this.setState({strategyData: null})
      })
  }

  async updateStrategies() {
    ApiUtil.getAllStrategiesFromClient1(this.state.openCompany).then(data =>
      this.setState ({
        strategyData: null,
        strategies: data,
        strategy: null,
        initialValue: null,
      }
    ))
  }

  render() {
    return (
      <div> 
        <div className={this.props.classes.root}>
        <CssBaseline />
        <NavigationBar
          updateStrategies={async () => await this.updateStrategies()}
          openCompany={this.state.openCompany} 
          open={this.state.open}
          handleDrawerOpen={() => {
            this.setState({open: true});
          }} 
          handleDrawerClose={() => {
            this.setState({open: false});
          }}
          classes={this.props.classes} />
        <Drawer
          variant="permanent"
          className={clsx(this.props.classes.drawer, {
            [this.props.classes.drawerOpen]: this.state.open,
            [this.props.classes.drawerClose]: !this.state.open,
          })}
          classes={{
            paper: clsx({
              [this.props.classes.drawerOpen]: this.state.open,
              [this.props.classes.drawerClose]: !this.state.open,
            }),
          }}
          open={this.state.open}
        >
          <div className={this.props.classes.toolbar}>
            <IconButton onClick={() => this.setState({open: false})}>
              {this.props.theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>
          <Divider />
          <AllStrategies classes={this.props.classes} 
            open={this.state.open} 
            handleCompanySelect={(company) => {
              this.setState({openCompany: company});
              this.handleCompanySelect(company)}}/>
          <Divider />
        </Drawer>
        <main className={this.props.classes.content}>
          <div className={this.props.classes.toolbar} />
          {
            this.state.openCompany ?
              <div>
                <CompanyStrategies 
                  updateStrategies={async () => await this.updateStrategies()}
                  handleStrategySelect={(strategy,initialValue) => {this.handleStrategySelect(strategy,initialValue)}} 
                  company={this.state.openCompany} 
                  strategies={this.state.strategies} />
                { 
                  this.state.strategyData ? 
                    <Summary type={"strategy"} 
                      strategy={this.state.strategy} 
                      strategyValues={this.state.strategyData}
                      initialValue={this.state.initialValue}>
                      </Summary>
                  :
                    <Summary type={"company"}></Summary>
                }
                
              </div>
            :
              <Summary 
                updateStrategies={async () => await this.updateStrategies()} 
                type={"portfolio"} 
                strategy={this.state.strategy}></Summary>
          }
        </main>
      </div>
    </div>
    );
  }
}

export default Container;
