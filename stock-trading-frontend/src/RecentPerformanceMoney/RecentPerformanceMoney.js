import React    from "react";
import Ring from 'ringjs';
import {Charts, BarChart, ScatterChart, Legend, Resizable, ChartContainer, ChartRow, YAxis, XAxis, styler} from 'react-timeseries-charts';

import {
  TimeSeries,
  TimeRange,
  TimeEvent,
  Pipeline as pipeline,
  Stream,
  EventOut,
  percentile
} from "pondjs";
import apiUtil from "../utility/ApiUtil";
import LineChart from "react-timeseries-charts/lib/components/LineChart";

const sec = 1000;
const minute = 60 * sec;
const hours = 60 * minute;
const rate = 80;

class RecentPerformanceMoney extends React.Component {
    static displayName = "AggregatorDemo";

    constructor(props) {
      super(props);
      this.state = {
        time: new Date(2015, 0, 1),
        events: new Ring(200),
        stream: new Stream(),
      }

      for (let i of props.strategyValues) {
        let date = new Date(i.datetTime);
        let volume = i.volume;
        let profit = i.sellPrice;
        let loss = i.buyPrice;
        let pnl = (profit-loss)*volume;
        if (this.state.events.toArray().length>0) {
          let length = this.state.events.toArray().length;
          pnl+=this.state.events.toArray()[length-1].toJSON().data.value;
        }
        let event = new TimeEvent(date, pnl);
        this.state.stream.addEvent(event);
        const temp = this.state.events;
        temp.push(event);
      }
    }

    async getNewEvent(t) {
      let newEvent;
      await apiUtil.getStrategyDetails(this.props.strategy).then((result) => {
        let previousLength = this.state.events.toArray().length;
        if (previousLength == result.length ||  result.length == 0) {
          newEvent = null;
        }
        else
        { 

          let last = result[result.length-1];
          let pnl = (last.sellPrice-last.buyPrice)*last.volume;
          if (this.state.events.length > 0) {
              pnl += this.state.events.toArray()[this.state.events.toArray().length-1].data.value
          }
          newEvent = new TimeEvent(new Date(result[result.length-1].datetTime), pnl);
        }
      });
      return newEvent;
    };

    async componentDidMount() {
        //
        // Setup our interval to advance the time and generate raw events
        //
        const increment = minute;
        this.interval = setInterval(async () => {
            const t = new Date(this.state.time.getTime() + increment);
            const event = await this.getNewEvent(t);

            // Raw events
            if (event != null) {
              const newEvents = this.state.events;
              newEvents.push(event);
              this.setState({ time: t, events: newEvents });
              this.state.stream.addEvent(event);
            }
            // Let our aggregators process the event
        }, rate);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        if (this.props.strategyValues.length > 0) {
        const latestTime = `${this.state.time}`;

        const fiveMinuteStyle = {
            value: {
                normal: { fill: "#619F3A", opacity: 0.2 },
                highlight: { fill: "619F3A", opacity: 0.5 },
                selected: { fill: "619F3A", opacity: 0.5 }
            }
        };

        const scatterStyle = {
            value: {
                normal: {
                    fill: "steelblue",
                    opacity: 0.5
                }
            }
        };

        //
        // Create a TimeSeries for our raw, 5min and hourly events
        //
        const eventSeries = new TimeSeries({
            name: "raw",
            events: this.state.events.toArray()
        });

        // Timerange for the chart axis
        const initialBeginTime = new Date(2015, 0, 1);
        const timeWindow = 20 * sec;
        let t1 = new Date(this.state.events.toArray()[0].timestampAsUTCString());
        let t2 = new Date(this.state.events.toArray()[this.state.events.toArray().length-1].timestampAsUTCString());

         
        
        const timeRange = new TimeRange(t1, t2);

        // Charts (after a certain amount of time, just show hourly rollup)
        const charts = (
            <Charts>
                <LineChart 
                  axis="y" 
                  series={eventSeries} 
                  style={scatterStyle} />
            </Charts>
        );

        const dateStyle = {
            fontSize: 12,
            color: "#AAA",
            borderWidth: 1,
            borderColor: "#F4F4F4"
        };
        return (
            <div>
                <div className="row">
                    <div className="col-md-8">
                        <span style={dateStyle}>{latestTime}</span>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-md-12">
                        <Resizable>
                            <ChartContainer timeRange={timeRange}>
                                <ChartRow height="150">
                                    <XAxis 
                                      id="x"
                                      label="Time Traded"
                                      format="dddd, mmmm dS, yyyy, h:MM:ss TT"
                                    />
                                    <YAxis
                                        id="y"
                                        label="Cashflow"
                                        min={-1500}
                                        max={1500}
                                        width="70"
                                        type="linear"
                                    />
                                    {charts}
                                </ChartRow>
                            </ChartContainer>
                        </Resizable>
                    </div>
                </div>
            </div>
        );  
    }
    else return (<div>Hello</div>)
}
}
export default RecentPerformanceMoney;