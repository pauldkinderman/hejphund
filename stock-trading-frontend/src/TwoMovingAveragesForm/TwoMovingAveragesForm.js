import React    from "react";

import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { Button, DialogTitle } from "@material-ui/core";
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import ApiUtil from '../utility/ApiUtil';

import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

class TwoMovingAveragesForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      strategy: "Two Moving Averages"
    }
  }

  handleChange = name => event => {
    this.setState({
      strategy: event.target.value,
    });
  };

  async handleSubmit() {
    if (!this.state.tickers)
    {
      await fetch('http://nyc31.conygre.com:31/Stock/getSymbolList').then(r => r.json())
      .then(r => {
        this.setState({ tickers: r});
      });
    }
    let found = false;
    let ticker = "";
    for(let i of this.state.tickers) {
      if (i.symbol.toUpperCase() == document.getElementById("formTicker").value) {
        ticker = i.symbol.toUpperCase();
        found = true;
      }
    }
    if (!found || 
      document.getElementById("stratName").value == "" || 
      document.getElementById("formVolume").value <= 0 ||
      document.getElementById("formTimePeriod").value <= 0) {
      this.props.openError()
      return;
    }
    else {
      let name = document.getElementById("stratName").value;
      let volume =  document.getElementById("formVolume").value;
      let time = document.getElementById("formTimePeriod").value;
      let type = "Two Moving Averages";
      let company; 
      await fetch("http://nyc31.conygre.com:31/Stock/getCompanyNameForSymbol/" + ticker).then(r => r.json()).then(r => company = r);
      await ApiUtil.createStrategy(company, ticker, name, volume, time, type);
      this.props.handleClose();
      await this.props.updateStrategies();
    }
  }
  render() {
    return (
      <div>
      <DialogContent>
          <DialogTitle>New Two Moving Averages Strategy</DialogTitle>
          <React.Fragment>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="stratName"
                name="stratName"
                label="Strategy Name"
                fullWidth
                autoComplete="stratName"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="formTicker"
                name="ticker"
                label="Ticker"
                fullWidth
                autoComplete="ticker"
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                required
                id="formVolume"
                name="volume"
                label="volume"
                fullWidth
                autoComplete="volume"
                type="number"
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                required
                id="formTimePeriod"
                name="timePeriod"
                label="Time Period (s)"
                fullWidth
                autoComplete="timePeriod"
                type="number"
              />
            </Grid>
          </Grid>
        </React.Fragment>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => this.handleSubmit()}>
            Submit Strategy
          </Button>
      </DialogActions>
      </div>
    );
  }
}

export default TwoMovingAveragesForm;
