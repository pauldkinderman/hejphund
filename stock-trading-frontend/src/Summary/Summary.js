import React    from "react";
import RecentPerformanceMoney from '../RecentPerformanceMoney';
import RecentPerformancePercent from '../RecentPerformancePercent';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import './Summary.css';
import { Box } from "@material-ui/core";
import apiUtil from "../utility/ApiUtil";

class Summary extends React.Component {
  constructor(props) {
    super(props);
  }
  async stopStrategy(strategy) {
    await apiUtil.stopStrategy(strategy);
    await this.props.updateStrategies();
  }
  render() {
    return (
      <div className={this.props.type+"SummaryContainer"}>
        {
          this.props.type==="strategy" ? 
            this.props.strategyValues.length  > 0 ?
                <Box boxShadow={5} maxWidth="xs">
                <RecentPerformanceMoney initialValue={this.props.initialValue} strategy={this.props.strategy} strategyValues={this.props.strategyValues} />
                <RecentPerformancePercent initialValue={this.props.initialValue} strategy={this.props.strategy} strategyValues={this.props.strategyValues} />
                <Button color="secondary" onClick={async () => await this.stopStrategy(this.props.strategy)}> Stop Strategy </Button>
                </Box>
              :
              <React.Fragment>
              <Box 
                boxShadow={5}
                maxWidth="xl">
                <Typography 
                  component="div" 
                  style={{ 
                    backgroundColor: '#efecea', 
                    height: '85vh',
                    }} >No transactions on current strategy</Typography>
              </Box>
              </React.Fragment>
          :
            <div> No strategy selected </div> 
        }
      </div>
    )
  }
}

export default Summary;
