import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import AllStrategies from './AllStrategies/AllStrategies'; //This import is needed for some reason
import useStyles from './stylingConstant/useSyles';
import Container from './Container/';



export default function App() {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Container classes={classes} theme={theme}/>
  );
}