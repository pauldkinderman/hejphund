import React from 'react';

import { List, ListItem, ListItemText, IconButton, ButtonBase, Grid} from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import AllStrategiesDetails from '../AllStrategiesDetails';
import './AllStrategies.css'
import ApiUtil from "../utility/ApiUtil";

class AllStrategies extends React.Component {
  constructor() {
    super();
    this.handleCompanySelect.bind(this);
    this.state = {
      strategies: [],
      openIndex: null,
      company: null
    }
  }

  componentDidMount() {
    ApiUtil.getAllStrategiesFromClient1()
    .then(data => {
      this.setState ({
        strategies:data
      });
    }).catch(e => console.log(e));
  }

  toggleDetails = (index) => (e) => {
    e.stopPropagation();
    if (this.state.openIndex !== index) this.setState({openIndex: index});
    else this.setState({openIndex: null});
  }
  
  handleCompanySelect(index) {
    this.props.handleCompanySelect(this.state.strategies[index].stockSymbol);
    this.setState({openIndex: index});
    this.setState({open: false});
  }

  render() {
    return (
      <List>
          {this.state.strategies.map((value, index) => (
              <div key={index} onClick={this.handleCompanySelect.bind(this, index)}>
                
                <ListItem button>
                <Grid container direction="row">
                  <Grid xs={6} item>
                      <ListItemText
                        className={this.props.classes.companyTicker} 
                        primary={value.stockSymbol} />
                </Grid>
                { this.props.open ? 
                <Grid xs={6} item container justify={"flex-end"}>
                    <Grid item>
                        <ListItemText 
                          className={this.props.classes.companySymbol}
                          secondary={value.stockCompany} />
                    </Grid>
                    <Grid item>
                    <IconButton size="small" onClick={this.toggleDetails(index)}>
                          {this.state.openIndex === index && this.props.open ? <ExpandLess /> : <ExpandMore/>}
                    </IconButton>
                  </Grid>
                </Grid>
                : <div></div>}
                </Grid>
              </ListItem>
              <AllStrategiesDetails values={value} open={this.state.openIndex===index && this.props.open} classes={this.props.classes} />
              </div>
          ))}
        </List>
    );
  }
}
export default AllStrategies;
                
