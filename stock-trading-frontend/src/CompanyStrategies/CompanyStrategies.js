import React    from "react";
import {List, ListItem, ListItemText, ListSubheader, Divider, Box} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import mockCompanyStrategies from '../mockData/mockCompanyStrategies';
import './CompanyStrategies.css';

class CompanyStrategies extends React.Component {
  
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div style={{float:"left"}}>
      <Box
        boxShadow={5}
        >
      <List
        classes={{root: "companyStrategies"}}
        subheader={
          <ListSubheader button component="div" id="nested-list-subheader" style={{color: "black", marginBottom:"20px"}}>
            <Typography variant="h6">Strategies for {this.props.company}</Typography>
          </ListSubheader>
        } >
        {this.props.strategies.map((strategy, index) => {
          return (
            <div key={index}>
            <ListItem button onClick={() => this.props.handleStrategySelect(strategy.id, strategy.initialValue)}>
              <ListItemText 
                color="primary" 
                classes={{primary: "companyPanel"}}
                secondary={strategy.strategyStatus !=="OPENED" && strategy.strategyStatus !=="RUNNING" ? 
                  "[Strategy Inactive]"
                : ""}>{strategy.strategyName}</ListItemText>
              <ListItemText 
                color="secondary" 
                component={"div"}
                secondary={
                  <div style={{textAlign: "center"}}>
                    <div>Strategy Type: {strategy.strategyType}</div>
                    <div>Shares Affected:{strategy.sharesAffected}</div>
                    <div>Amount PNL: ${strategy.dollarAmountPNL}</div>
                    <div>PNL %: {strategy.percentPNL}</div>
                    <div>Dollar Amount PNL: {strategy.dollarAmountPNL}</div>
                  </div>
                }
              />
            </ListItem>
            <Divider color="primary"/>
            </div>
          )
        })}
    </List></Box></div>
    )
  }
}

export default CompanyStrategies;