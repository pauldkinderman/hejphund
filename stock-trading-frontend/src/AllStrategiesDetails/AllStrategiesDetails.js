import React    from "react";
import { Collapse, List, ListItem, ListItemText } from '@material-ui/core';

class AllStrategiesDetails extends React.Component {
  
  render() {
    return(
      <Collapse in={this.props.open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem className={this.props.classes.nested}>
            <ListItemText secondary={"Net PNL: " + this.props.values.netDollarPNL} />
          </ListItem>
          <ListItem className={this.props.classes.nested}>
            <ListItemText secondary={"Net PNL %: " + (this.props.values.netPercentPNL ? this.props.netPercentPNL : "N/A")} />
          </ListItem>
          <ListItem className={this.props.classes.nested}>
            <ListItemText secondary={"Total Stocks Affected: " + this.props.values.netShare} />
          </ListItem>
          <ListItem className={this.props.classes.nested}>
            <ListItemText secondary={"Number of Strategies: " + this.props.values.strategyCount} />
          </ListItem>
        </List>
      </Collapse>
    )
  }
}

export default AllStrategiesDetails;
