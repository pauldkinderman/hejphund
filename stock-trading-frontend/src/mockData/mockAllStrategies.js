const mockAllStrategies = [
    {companyName: "Citi", ticker: "C", netPNL: 400, netPercentPNL: .03, netStocks: -200, numberOfStrats: 5},
    {companyName: "Apple", ticker: "AAPL", netPNL: -300, netPercentPNL: -.05, netStocks: 150, numberOfStrats: 9},
    {companyName: "Microsoft", ticker: "MSFT", netPNL: 0, netPercentPNL: 0, netStocks: 0, numberOfStrats: 17},
];
export default mockAllStrategies;