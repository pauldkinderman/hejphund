
class ApiUtil{

    async getAllStrategiesFromClient1(){
        return await fetch(`${process.env.REACT_APP_BASEURL}/strategies/1`).then(r => r.json());
    }

    async getAllStrategiesFromSpecificCompany(companySym) {
        return await fetch(`${process.env.REACT_APP_BASEURL}/strategies/1/${companySym}`).then(r => r.json())
    }

    async createStrategy(companyName, companySym, stratName, volume, time, stratType) {
        return await fetch(`${process.env.REACT_APP_BASEURL}/strategies`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    companyName: companyName,
                    stockTickerSymbol: companySym,
                    volume: volume,
                    clientId: 1,
                    strategyAlias: stratName,
                    timePeriod: time,
                    strategyType: stratType,
                })
        });
    }

    async getStrategyDetails(strategy) {
        return await fetch(`${process.env.REACT_APP_BASEURL}/transactions/1/strategy/${strategy}`).then(r=> r.json());
    }
    async stopStrategy(strategy) {
        return await fetch(`${process.env.REACT_APP_BASEURL}/strategies/1/stopStrategy/${strategy}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id: strategy,
                status: "EXITED"
            }),
        }).then(r=>r.json());
    }
}

const apiUtil = new ApiUtil(process.env.REACT_APP_baseUrl);
export default apiUtil;