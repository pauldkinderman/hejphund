import React    from "react";
import clsx from 'clsx';

import { AppBar, IconButton, Typography, Toolbar } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu'
import StrategyForm from "../StrategyForm/StrategyForm";

class NavigationBar extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    
    return (
      <AppBar
        position="fixed"
        className={clsx(this.props.classes.appBar, {
          [this.props.classes.appBarShift]: this.props.open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={this.props.handleDrawerOpen}
            edge="start"
            className={clsx(this.props.classes.menuButton, {
              [this.props.classes.hide]: this.props.open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            {this.props.openCompany ? this.props.openCompany : "My Portfolio"}
          </Typography>

            <StrategyForm updateStrategies={async () => await this.props.updateStrategies()} classes={this.props.classes} />

        </Toolbar>
      </AppBar>
    )
  }
}

export default NavigationBar;
