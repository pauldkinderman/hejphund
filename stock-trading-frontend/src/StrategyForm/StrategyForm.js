import React    from "react";
import Typography from '@material-ui/core/Typography';
import { Button, DialogTitle, Fab, ButtonGroup } from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
import Snackbar from '@material-ui/core/Snackbar';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';

import clsx from 'clsx';

import ApiUtil from '../utility/ApiUtil'
import TwoMovingAveragesForm from "../TwoMovingAveragesForm/TwoMovingAveragesForm.js";


class StrategyForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      errorOpen: false,
      tickers: null,
      formType: "Two Moving Averages",
      formNumber: 0,
    }
  }
  a11yProps(index) {
    return {
      id: `action-tab-${index}`,
      'aria-controls': `action-tabpanel-${index}`,
    };
  }
  handleChange(event){
    this.setState({
      formType: event.target.innerHTML,
      formNumber: event.target.id,
    });
  };

  handleClose() {
    this.setState({open: false});
  }

  handleOpen() {
    this.setState({open:true});
  }

  render() {
    return(
      <div>
      <Fab variant="extended" size="medium" color="primary" className={this.props.classes.newStrategyButton} onClick={()=>this.setState({open: true})}><Typography noWrap>New Strategy</Typography></Fab>
      <Dialog
        onClose={() => this.setState({open: false})} 
        aria-labelledby="simple-dialog-title" 
        open={this.state.open}>
          <AppBar position="static" color="default">
            <ButtonGroup
                fullWidth
                variant="contained"
                color="primary"
                aria-label="full-width contained primary button group"
              >
                <Button onClick={()=> this.setState({formNumber: 0})}>Two Moving Averages</Button>
              </ButtonGroup>
          </AppBar>
          {
            this.state.formNumber == 0 ?
              <TwoMovingAveragesForm 
                handleClose={() => this.handleClose()} openError={() => this.setState({errorOpen:true})}
                updateStrategies={async () => await this.props.updateStrategies()}/>
            :
              <div>other forms</div>
          }
          
      </Dialog>
      <Snackbar
        anchorOrigin={{ 
           vertical: "bottom",
           horizontal : "right" }}
        open={this.state.errorOpen}
        onClose={() => this.setState({errorOpen: false})}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={"Invalid Strategy"}
      />
    </div>
    )
  }

}

export default StrategyForm;