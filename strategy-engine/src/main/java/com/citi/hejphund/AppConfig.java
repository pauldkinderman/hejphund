package com.citi.hejphund;

import com.citi.hejphund.messaging.BrokerClient;
import com.citi.hejphund.strategy.StrategyManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.jms.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@EnableJms
@EnableScheduling
@Configuration
@SpringBootApplication
public class AppConfig {


    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(AppConfig.class, args);
        //BrokerClient client = context.getBean(BrokerClient.class);

        StrategyManager strategyManager = context.getBean(StrategyManager.class);
        strategyManager.startTradingNewStrategies();
    }

    @Bean
    public Queue queueFromBroker() { return new ActiveMQQueue("executedTransaction.queue"); }
    @Bean
    public Queue queueToBroker() {
        return new ActiveMQQueue("mockBroker.queue");
    }

//    @Bean
//    public BrokerClient brokerClient() { return new BrokerClient(); }

    @Bean
    public StrategyManager strategyManager() {
        return new StrategyManager();
    }


}
