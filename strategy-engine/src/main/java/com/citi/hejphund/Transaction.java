package com.citi.hejphund;

import com.citi.hejphund.strategy.Strategy;
import com.fasterxml.jackson.annotation.JsonFormat;
import javafx.util.converter.LocalDateTimeStringConverter;
import org.springframework.format.annotation.DateTimeFormat;
//import org.apache.tomcat.jni.Local;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Transaction implements Serializable {
    Integer tid;
    Integer strategyId;
    String stockTickerSymbol;
    Double buyPrice;
    Double sellPrice;
    Integer volume;
    Double pnl;
//    LocalDateTime dateTime;
    String dateTime;
    private Double percentPnl;

    public Transaction() {
    }
    public Transaction(
            Integer strategyId,
            String stockTickerSymbol,
            Double buyPrice,
            Double sellPrice,
            Integer volume,
            Double pnl,
            String dateTime
    ){
        this.strategyId = strategyId;
        this.stockTickerSymbol = stockTickerSymbol;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.volume = volume;
        this.pnl = pnl;
        this.dateTime = dateTime;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getStrategyId() {
        return this.strategyId;
    }

    public void setStrategyId(Integer strategyId) {
        this.strategyId = strategyId;
    }

//    public Strategy getStrategy() {
//        return strategy;
//    }
//
//    public void setStrategy(Strategy strategy) {
//        this.strategy = strategy;
//    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public String getStockTickerSymbol() {
        return stockTickerSymbol;
    }

    public void setStockTickerSymbol(String stockTickerSymbol) {
        this.stockTickerSymbol = stockTickerSymbol;
    }

    public Double getPnl() {
        return pnl;
    }

    public void setPnl(Double pnl) {
        this.pnl = pnl;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Double getPercentPnl() {
        return percentPnl;
    }

//    public LocalDateTime getDateTime() { return dateTime; }
//    public void setDateTime(LocalDateTime dateTime){this.dateTime = dateTime;}
}
