package com.citi.hejphund.messaging;

import com.citi.hejphund.Transaction;
import com.citi.hejphund.strategy.Strategy;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.Queue;
import java.io.IOException;
import java.util.logging.Logger;

@Component
public class MockBroker {

    Logger logger = Logger.getLogger(MockBroker.class.getName());

    Gson gson = new Gson();

    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private Queue queueFromBroker;

    private void sendExecutedTransaction(Transaction transaction) throws JsonProcessingException {
        String transactionJson = gson.toJson(transaction);
        MessageCreator messageCreator = session -> session.createTextMessage(transactionJson);

        jmsTemplate.send(queueFromBroker, messageCreator);
    }


    @JmsListener(destination = "mockBroker.queue")
    public void processIncomingTransaction(String message) {

        Transaction transaction;
        try {
            transaction = gson.fromJson(message, Transaction.class);
            logger.info("Received transaction json: " + message);
            logger.info("Processing transaction # " + transaction.toString());
            sendExecutedTransaction(transaction);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
