package com.citi.hejphund.messaging;

import com.citi.hejphund.Transaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.Session;
import java.io.IOException;
import java.util.logging.Logger;

@Component
public class BrokerClient {

    private Logger log = Logger.getLogger(BrokerClient.class.getName());
    private Gson gson = new Gson();

    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private Queue queueToBroker;

    public JmsTemplate getJmsTemplate() {
        return jmsTemplate;
    }

    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public Queue getQueueToBroker() {
        return queueToBroker;
    }

    public void setQueueToBroker(Queue queueToBroker) {
        this.queueToBroker = queueToBroker;
    }

    public void sendTransaction(Transaction transaction)  {
        // Create a message
        String transactionJson = gson.toJson(transaction);
        MessageCreator messageCreator = session -> session.createTextMessage(transactionJson);

        log.info("Sending Transaction json: " + transactionJson);
        jmsTemplate.send(queueToBroker, messageCreator);
    }

}
