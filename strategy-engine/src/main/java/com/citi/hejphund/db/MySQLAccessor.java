package com.citi.hejphund.db;

import com.citi.hejphund.Transaction;
import com.citi.hejphund.strategy.BollingerBandsStrategy;
import com.citi.hejphund.strategy.Strategy;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.citi.hejphund.strategy.TwoMovingAveragesStrategy;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.cglib.core.Local;

/**
 * The role of this class is to provide read/write/update access to the MySQL database.
 * Author: Evelyn Galvan
 */
public class MySQLAccessor {
    private static Logger log = LogManager.getLogger(MySQLAccessor.class);
    private Connection connection = null;
    private ResultSet resultSet = null;

    public MySQLAccessor(){
        try {
            //BasicConfigurator.configure();
            establishConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void establishConnection() throws ClassNotFoundException, SQLException {
        // loads mysql driver
        Class.forName("com.mysql.jdbc.Driver");

        connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/hejphund"
                        +"?user=root&password=c0nygre&serverTimezone=UTC");
    }


    /**
     * @returns open strategies
     * @throws SQLException
     */
    public List<Strategy> getStrategiesWithStatusOpened() throws SQLException {
        PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT * FROM hejphund.strategies s WHERE s.strategy_status = 'OPENED'");

        return getStrategiesFromQueryResult(preparedStatement);
    }

    public List<Strategy> getStrategiesWithStatusRunning() throws SQLException {
        PreparedStatement preparedStatement = connection
                .prepareStatement("SELECT * FROM hejphund.strategies s WHERE s.strategy_status = 'RUNNING'");

        return getStrategiesFromQueryResult(preparedStatement);
    }

    public List<Strategy> getStrategiesFromQueryResult(PreparedStatement preparedStatement) throws SQLException {
        List<Strategy> strategiesList = new ArrayList<>();
        Integer clientId, strategyId, totalTransactionVolume, timePeriod;
        String stockCompany, stockSymbol, strategyAlias, strategyType, strategyStatus;
        Double pnLPercentage, pnLInDollars;
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getResultSet();

        while(rs.next()){
            strategyId = rs.getInt("id");                       log.info("Strategy Id: " + strategyId);
            clientId = rs.getInt("client_id");                  log.info("Client Id: " + clientId);
            strategyAlias = rs.getString("strategy_alias");     log.info("Strategy Name: "+strategyAlias);
            strategyType = rs.getString("strategy_type");       log.info("Strategy: " + strategyType);
            stockCompany = rs.getString("stock_company");       log.info("Company: " + stockCompany);
            stockSymbol = rs.getString("stock_symbol");         log.info("Stock: " + stockSymbol);
            totalTransactionVolume = rs.getInt("total_transaction_volume"); log.info("Volume: "+totalTransactionVolume);
            strategyStatus = rs.getString("strategy_status");   log.info("Strategy: " + strategyStatus);
            pnLPercentage = rs.getDouble("percent_PNL");        log.info("P&L: " + pnLPercentage + "%");
            pnLInDollars = rs.getDouble("dollar_PNL");          log.info("P&L: $" + pnLInDollars);
            timePeriod = rs.getInt("time_period");              log.info("timePeriod: " + timePeriod);

            Strategy strategy = null;
            if(strategyType.equals("TWO_MOVING_AVERAGES")){
                strategy = new TwoMovingAveragesStrategy(clientId, strategyId, strategyAlias, stockSymbol, totalTransactionVolume,
                        strategyStatus, timePeriod);
            }
            if(strategyType.equals("BOLLINGER_BANDS")){
                strategy = new BollingerBandsStrategy(
                        clientId,
                        strategyId,
                        strategyAlias,
                        stockSymbol,
                        totalTransactionVolume,
                        strategyStatus,
                        timePeriod
                );
            }
            strategiesList.add(strategy);
            System.out.println("-----------------------");
        }
        return strategiesList;
    }
    public void setStrategyStatusAsRunning(Strategy strategy) throws SQLException {
        PreparedStatement preparedStatement;
        preparedStatement =
                connection.prepareStatement(
                        "UPDATE hejphund.strategies s " +
                                "SET s.strategy_status = 'RUNNING' " +
                                "WHERE s.id =" + strategy.getStrategyId());

        preparedStatement.execute();
    }

    public void writeNewTransactionToDB(Transaction transaction) throws SQLException {
        PreparedStatement preparedStatement;
//        LocalDateTime localDateTime = LocalDateTime.parse(transaction.getDateTime());
        preparedStatement =
                connection.prepareStatement("INSERT INTO hejphund.transactions VALUES" +
                        "("
                        + transaction.getTid() + ", "
                        + transaction.getStrategyId() + ", "
                        + "'" + transaction.getStockTickerSymbol() + "', "
                        + transaction.getBuyPrice() + ", "
                        + transaction.getSellPrice() + ", "
                        + transaction.getVolume() + ", "
                        + transaction.getPnl() + ", "
                        + "'" + transaction.getDateTime() + "')"
                );
        preparedStatement.execute();
    }
}
