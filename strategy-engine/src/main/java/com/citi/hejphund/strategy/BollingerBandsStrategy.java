package com.citi.hejphund.strategy;
import com.citi.hejphund.feed.FeedGrabber;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
public class BollingerBandsStrategy extends Strategy {
    
    //find moving average
    //find standard deviation
    //if price hits some multitude of st.dev on low side then go for long
    //if price hits some multitude of st.dev on high side then go for short
    //exit position if 10% loss or 1% gain

    List<Double> priceList = new ArrayList<>();
    private double movingAverage;
    private Integer period = 11000;
    private Integer numTrans = 0;


    private double sum;
    private double mean;
    private double stdev;
    private double dev;
    private double diff;
    private double diffsquared;
    private double deviation;
    private String move;
    private String stocktickersymbol;
    FeedGrabber feedgrabber;
    private Boolean lastDecisionIsBuy = null;
    private boolean fivePercentLost = false;
    private boolean OnePercentGain = false;


    public BollingerBandsStrategy(int clientId,
                                  int strategyId,
                                  String strategyAlias,
                                  String stockTickerSymbol,
                                  int transactionVolume,
                                  String strategyStatus,
                                  int timePeriod){
        super(strategyId,
                clientId,
                strategyAlias,
                stockTickerSymbol,
                transactionVolume,
                strategyStatus,
                timePeriod);
        this.clientId = clientId;
        this.strategyAlias = strategyAlias;
        this.stockTickerSymbol = stockTickerSymbol;
        this.transactionVolume = transactionVolume;
        this.strategyStatus = strategyStatus;
        this.timePeriod = timePeriod;
        this.period = period;
        feedgrabber = new FeedGrabber();
    }

    public Double getCalculateStdForPeriod() throws IOException {
        List<Double> prices = feedgrabber.getStockPriceList(stockTickerSymbol, period);
        return calcStDev(prices);
    }

//    public String makePeriod (double num) {
//        priceList.add(num);
//        if (priceList.size() > period + 1) {
//            priceList.remove(0);
//            deviation = calculationFunc(priceList);
//        }
//        //move = checkToMakeMove(deviation, num);
//        return "sweet";
//    }

//    public double calculationFunc(List<Double> stockList) {
//        for (int i = 0; i <= stockList.size()-period; i++) {
//            dev = calcStDev(stockList);
//        }
//        return 1;
//    }

    public Double calcStDev(List<Double> prices) {
        for (int i = 0; i < prices.size(); i++) {
            sum += prices.get(i);
            mean = sum / prices.size();
            diffsquared =0;
        }
        for (int i = 0; i < prices.size(); i++) {
            diff = mean-prices.get(i);
            diffsquared += Math.pow(diff, 2);
        }
        stdev = Math.sqrt(diffsquared/(period+1));
        return stdev;
    }

//    public void checkToMakeMove(double deviation, double num) {
//        //if price hits some multitude of st.dev on low side then go for long
//        //if price hits some multitude of st.dev on high side then go for short
//        //exit position if 10% loss or 1% gain
//
//
//        if (num > deviation*2 & num < deviation*3) {
////            System.out.println("go for short");
////            return "go to short";
//            //buy
//        } else if ((num -deviation*2)<0 & num < deviation*3) { //fix this conditional
////            System.out.println("go for long");
////            return "go for long";
//            //sell
//        } else {
////            System.out.println("do nothing");
////            return "do nothing";
//        }
//    }

    @Override
    public void start() throws IOException {
        while(!fivePercentLost && !OnePercentGain){
            makeDecision(); // long or short
        }

    }

    private void makeDecision() throws IOException {
        Double currPrice = feedgrabber.getStockPrice(stockTickerSymbol);
        Double std = getCalculateStdForPeriod();
        if( (std >= currPrice) && (lastDecisionIsBuy == null || !lastDecisionIsBuy)){ // buy
            numTrans++;
            System.out.println(numTrans + ": buying at " + currPrice);
            lastDecisionIsBuy = true;
        }
        if((std <= currPrice && (lastDecisionIsBuy == null || lastDecisionIsBuy))){
            numTrans++;
            System.out.println(numTrans+": shorting at " + currPrice);
            lastDecisionIsBuy = false;
        }

    }
}