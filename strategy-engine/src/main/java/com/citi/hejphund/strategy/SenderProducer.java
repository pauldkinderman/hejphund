package com.citi.hejphund.strategy;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.jms.*;


public class SenderProducer {

//    private Logger logger = LogManager.getLogger(SenderProducer.class);

    private String clientId;
    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private Destination destination;
    private MessageProducer messageProducer;

    public SenderProducer(String queueName) {
        try {
            connectionFactory = new ActiveMQConnectionFactory((ActiveMQConnection.DEFAULT_BROKER_URL));
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(false, session.AUTO_ACKNOWLEDGE);
            destination = session.createQueue("TestQueue");
            messageProducer = session.createProducer(destination);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

//    public void createMessageQueue(String clientId, String queueName) throws JMSException {
//        this.clientId = clientId;
//
//        // create a Connection Factory
//        ConnectionFactory connectionFactory =
//                new ActiveMQConnectionFactory(
//                        ActiveMQConnection.DEFAULT_BROKER_URL
//                );
//
//        // create connection
//        connection = connectionFactory.createConnection();
//        connection.setClientID(clientId);
//
//        // create a session
//        session =
//                connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//
//        //create the Queue to which messages will be sent
//        Queue queue = session.createQueue(queueName);
//
//        // create a MessageProducer for sending messages
//        messageProducer = session.createProducer(queue);
//    }

    public void closeConnection() throws JMSException {
        connection.close();
    }

    public void sendString(String message) throws JMSException {

        // create a JMS TextMessage
        TextMessage textMessage = session.createTextMessage(message);

        // send the message to the queue destination
        messageProducer.send(textMessage);

//        logger.info(clientId + ": sent message with text='{}'" + message);
    }

}
