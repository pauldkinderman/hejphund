package com.citi.hejphund.strategy;

import com.citi.hejphund.Transaction;
import com.citi.hejphund.feed.FeedGrabber;
import com.google.gson.Gson;
import javafx.util.Pair;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TwoMovingAveragesStrategy extends Strategy {

    private int transactionVolume;
    private Logger log = LogManager.getLogger(TwoMovingAveragesStrategy.class);
    private FeedGrabber feedGrabber;
    private Gson gson = new Gson();
    private List<Double> prices;

    private static Double livePrice;
    private double weightedAverage;
    private double shortAverage, longAverage;
    private int timePeriod;
    private int readEveryNSeconds;
    private double currentTransactionGains = 0.0;
    private double currentBuyPrice, currentSellPrice = 0.0;
    private double firstBuyInDollars;
    private double totalStrategyGains = 0.0;

    //false implies the last decision was a sell
    private Boolean lastDecisionIsBuy = null;
    private boolean isFirstTransaction = false;

    public TwoMovingAveragesStrategy(int clientId,
                                     int strategyId,
                                     String strategyAlias,
                                     String stockTickerSymbol,
                                     int transactionVolume,
                                     String strategyStatus,
                                     int timePeriod)
    {

        super(strategyId,
                clientId,
                strategyAlias,
                stockTickerSymbol,
                transactionVolume,
                strategyStatus,
                timePeriod);
        //BasicConfigurator.configure();
        this.clientId = clientId;
        this.strategyAlias = strategyAlias;
        this.stockTickerSymbol = stockTickerSymbol;
        this.transactionVolume = transactionVolume;
        this.strategyStatus = strategyStatus;
        this.timePeriod = timePeriod;
        readEveryNSeconds = 1;
        tenPercentLoss = false;
        onePercentGain = false;

        prices = new ArrayList<>();
        feedGrabber = new FeedGrabber();
    }

    /**
     * newest 25% of prices are weighed more at a rate of 1 + 0.2x
     * while the other older 75% of prices are weighed at 1
     * @return weighted average
     * @throws IOException
     */
    public Double getWeightedAverage() throws IOException {
        //Integer amountOfReads = timePeriod/ readEveryNSeconds;
        //Integer amountOfPricesToWeigh;
        //Double priceSum, weightedPortionPriceSum = 0.0;

        // gather all prices
        prices = feedGrabber.getStockPriceList(stockTickerSymbol, 2);
        // get current price
        livePrice = prices.get(prices.size()-1);

        List<Double> newPricesList = IntStream.range(0, prices.size())
                .filter(n -> n % readEveryNSeconds == 0)
                .mapToObj(prices::get)
                .collect(Collectors.toList());

        //amountOfPricesToWeigh = Math.toIntExact(Math.round(prices.size()*.25));
        // gather the most recent 25% of prices
        //List<Double> pricesToWeigh = prices.stream().skip(prices.size()-amountOfPricesToWeigh).collect(Collectors.toList()); // last 25% of prices
        //priceSum = prices.stream().mapToDouble(p -> p).sum();
        List<Pair<Double, Double>> priceWeightPairs = newPricesList.stream().map((d) -> new Pair<Double, Double>(d, 1d)).collect(Collectors.toList());
        int n = 1;
        for (int i = Math.toIntExact(Math.round((double) priceWeightPairs.size() * 0.75)); i < priceWeightPairs.size(); i++) {
            final Pair<Double, Double> doubleDoublePair = priceWeightPairs.get(i);
            priceWeightPairs.set(i, new Pair<>(doubleDoublePair.getKey(),
                    1 + (0.3*(n++))
            ));
        }
        double totalWeight = priceWeightPairs.stream().mapToDouble(Pair::getValue).sum();
        double totalWeightedPrices = priceWeightPairs.stream().mapToDouble((p) -> p.getKey()*p.getValue()).sum();
        double weightedAverage = totalWeightedPrices / totalWeight;

        return weightedAverage;
    }

    // possibly every second
    public void updateCurrentState(){}

    // assuming only long positions (buy first then sell)
    public void start() throws IOException {

        int numTrades = 0;
        boolean completeTransaction = false;

        while(!tenPercentLoss || !onePercentGain){
            //log.info(getLivePrice());
            if (getLivePrice() > getWeightedAverage()
                    && ((completeTransaction || numTrades==0 || !lastDecisionIsBuy) && currentSellPrice>getLivePrice())){ //buy
                numTrades++;
                if(numTrades == 1){ isFirstTransaction = true; }
                buy();
                completeTransaction = isCompleteTransaction(numTrades);
            }

            if (getLivePrice() < getWeightedAverage() && ((completeTransaction || numTrades==0 || lastDecisionIsBuy) && getLivePrice()>currentBuyPrice) ){ //sell
                numTrades++;
                if(numTrades == 1){ isFirstTransaction = true; }
                sell();
                completeTransaction = isCompleteTransaction(numTrades);

                if(getPnLRatio() <= 0.10 && !isFirstTransaction){
                    tenPercentLoss = true;
                }
                if(getPnLRatio() >= 0.01){
                    onePercentGain = true;
                }

            }
            //else{
//                if(numTrades>0 && ( completeTransaction || lastDecisionIsBuy )){
//                    sell();
//                    numTrades++;
//                    completeTransaction = isCompleteTransaction(numTrades);
//                }else if (getLivePrice() > getWeightedAverage() && numTrades>0 && (completeTransaction || !lastDecisionIsBuy )){
//                    buy();
//                    numTrades++;
//                    completeTransaction = isCompleteTransaction(numTrades);
                }
            }

    private void sell() {
        lastDecisionIsBuy = false;
        currentSellPrice = livePrice;
        log.info("Sell "
                + transactionVolume + " shares of "
                + stockTickerSymbol + " stock at "
                + currentSellPrice);
    }

    private boolean isCompleteTransaction(int numTrades) {
        boolean completeTransaction;
        if (numTrades % 2 == 0) {
            updatePnL(currentBuyPrice, currentSellPrice);
            log.info("The current PnL is " + totalStrategyGains);
            completeTransaction = true;

            Transaction transaction =
                    new Transaction(
                            this.getStrategyId(),
                            this.stockTickerSymbol,
                            currentBuyPrice,
                            currentSellPrice,
                            this.transactionVolume,
                            currentTransactionGains,
                            LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                    );
            brokerClient.sendTransaction(transaction);
        } else {
            completeTransaction = false;
        }
        return completeTransaction;
    }

    private double getLivePrice() throws IOException {
        prices = feedGrabber.getStockPriceList(stockTickerSymbol, timePeriod);
        // get current price
        return prices.get(prices.size()-1);
    }

//    public void start2() throws IOException {
//        int numBuys = 0;
//        while(!tenPercentLoss || !onePercentGain){
//            weightedAverage = getWeightedAverage();
//            shortAverage = getShortAverage();
//            if (shortAverage > weightedAverage && !lastDecisionIsBuy){ //buy
//                numBuys++;
//                if(numBuys == 1){ isFirstTransaction = true; }
//                buy();
//            }
//            if (shortAverage < weightedAverage && lastDecisionIsBuy) { //sell
//                sell();
//                // create json
//                Transaction transaction =
//                        new Transaction(
//                                this.getStrategyId(),
//                                this.stockTickerSymbol,
//                                currentBuyPrice,
//                                currentSellPrice,
//                                this.transactionVolume,
//                                currentTransactionGains,
//                                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
//                        );
//                brokerClient.sendTransaction(transaction);
//            }
//        }
//    }

    private void buy() {
        lastDecisionIsBuy = true;
        currentBuyPrice = livePrice;
        log.info("Buy "
                + transactionVolume + " shares of "
                + stockTickerSymbol + " stock at "
                + currentBuyPrice);
    }


    private double getShortAverage() throws IOException {
        prices = feedGrabber.getStockPriceList(stockTickerSymbol, timePeriod);
        // get current price
        livePrice = prices.get(prices.size()-1);
        // gather the most recent 25% of prices
        List<Double> pricesToWeigh = prices.stream().skip(prices.size()-Math.toIntExact(Math.round(prices.size()*.15))).collect(Collectors.toList()); //

        double totalPrices = pricesToWeigh.stream().mapToDouble(p -> p).sum();
        shortAverage = totalPrices / pricesToWeigh.size();

        return shortAverage;

    }

    private void updatePnL(Double buyPrice, Double sellPrice) {
        currentTransactionGains = getTransactionGains(buyPrice, sellPrice);
        totalStrategyGains += currentTransactionGains;
        pnlDollars = totalStrategyGains;

        //setPnLRatio(totalStrategyGains);

    }


    public double getTransactionGains(Double buyPrice, Double sellPrice){
        return ( (sellPrice*transactionVolume) - (buyPrice*transactionVolume) );
    }

    private void setPnLRatio(double pnlRatio){ this.pnlRatio = pnlRatio; }
    private double getPnLRatio() {
        return totalStrategyGains /(firstBuyInDollars*transactionVolume);
    }

    public Double getPnl() {
        return totalStrategyGains;
    }

    public Double getPercentPnl() {
        return getPnLRatio();
    }

}
