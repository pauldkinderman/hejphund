package com.citi.hejphund.strategy;

import java.io.IOException;
import com.citi.hejphund.messaging.BrokerClient;

public abstract class Strategy {
    protected String strategyAlias, stockTickerSymbol, strategyStatus, strategyType;
    protected Double pnlRatio, pnlDollars;
    protected Integer transactionVolume;
    protected Integer clientId, strategyId, timePeriod;
    private Integer readEverySecond;
    protected boolean tenPercentLoss, onePercentGain;

//    protected SenderProducer messageProducer;
    protected BrokerClient brokerClient;

    public Strategy() {
    }

    public Strategy(Integer strategyId,
                    Integer clientId,
                    String strategyAlias,
                    //String strategyType,
                    String stockTickerSymbol,
                    Integer transactionVolume,
                    String strategyStatus,
                    Integer timePeriod
                    )
    {
        this.strategyId = strategyId;
        this.clientId = clientId;
        this.strategyAlias = strategyAlias;
        //this.strategyType = strategyType;
        this.stockTickerSymbol = stockTickerSymbol;
        this.transactionVolume = transactionVolume;
        this.strategyStatus = strategyStatus;
        this.timePeriod = timePeriod;
    }
    public double getTransactionGains(Double buyPrice, Double sellPrice){return 0.0;}
    public String getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(String strategyType) {
        this.strategyType = strategyType;
    }

    public Integer getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(Integer timePeriod) {
        this.timePeriod = timePeriod;
    }

    public abstract void start() throws IOException;

    public String getStrategyAlias() {
        return strategyAlias;
    }

    public void setStrategyAlias(String strategyAlias) {
        this.strategyAlias = strategyAlias;
    }

    public String getStockTickerSymbol() {
        return stockTickerSymbol;
    }

    public void setStockTickerSymbol(String stockTickerSymbol) {
        this.stockTickerSymbol = stockTickerSymbol;
    }

    public String getStrategyStatus() {
        return strategyStatus;
    }

    public void setStrategyStatus(String strategyStatus) {
        this.strategyStatus = strategyStatus;
    }

    public Double getPnlRatio() {
        return pnlRatio;
    }

    public void setPnlRatio(Double pnlRatio) {
        this.pnlRatio = pnlRatio;
    }

    public Double getPnlDollars() {
        return pnlDollars;
    }

    public void setPnlDollars(Double pnlDollars) {
        this.pnlDollars = pnlDollars;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Integer strategyId) {
        this.strategyId = strategyId;
    }

    public Integer getTransactionVolume() {
        return transactionVolume;
    }

    public void setTransactionVolume(Integer transactionVolume) {
        this.transactionVolume = transactionVolume;
    }

    public boolean isTenPercentLoss() {
        return tenPercentLoss;
    }

    public void setTenPercentLoss(boolean tenPercentLoss) {
        this.tenPercentLoss = tenPercentLoss;
    }

    public boolean isOnePercentGain() {
        return onePercentGain;
    }

    public void setOnePercentGain(boolean onePercentGain) {
        this.onePercentGain = onePercentGain;
    }

    public BrokerClient getBrokerClient() {
        return brokerClient;
    }

    public void setBrokerClient(BrokerClient brokerClient) {
        this.brokerClient = brokerClient;
    }
    public void start2() throws IOException {};

    //    public SenderProducer getMessageProducer() {
//        return messageProducer;
//    }
//
//    public void setMessageProducer(SenderProducer messageProducer) {
//        this.messageProducer = messageProducer;
//    }
}
