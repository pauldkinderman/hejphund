package com.citi.hejphund.strategy;

import com.citi.hejphund.Transaction;
import com.citi.hejphund.db.MySQLAccessor;
import com.citi.hejphund.messaging.BrokerClient;
import com.google.gson.Gson;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.Scheduled;

import javax.jms.MessageProducer;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class retrieves new open strategies from the MySQLAccessor
 * triggers the respective strategies.
 *
 * It receives buy/sell decisions from Strategy
 * then sends a transaction request to the MockBroker for approval.
 */
public class StrategyManager {

    private Logger log = Logger.getLogger(StrategyManager.class.getName());

    @Autowired
    private BrokerClient brokerClient;
    private Gson gson = new Gson();

    private List<Strategy> strategyList;
    private MySQLAccessor db;

    public StrategyManager() {
        BasicConfigurator.configure();
        this.strategyList = new ArrayList<>();
        db = new MySQLAccessor();
//        messageProducer = new SenderProducer("TestQueue");
    }

    public void setBrokerClient(BrokerClient brokerClient) {
        this.brokerClient = brokerClient;
    }

    public void startTradingNewStrategies2(){
        try {
            List<Strategy> openStrategies = db.getStrategiesWithStatusOpened();
            openStrategies.forEach(s -> {
                    s.setBrokerClient(this.brokerClient);
                try {
                    s.start2();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
            log.warn("Could not get strategies with status opened from db");
        }
    }

    @Scheduled(fixedRate = 3000)
    public void startTradingNewStrategies() { //comment
        try {
            log.info("Looking for new opened strategies");
            List<Strategy> openStrategies = db.getStrategiesWithStatusOpened();
            if (openStrategies.size() > 0) {
                openStrategies.forEach(s -> {
                    try {
                        log.info("Starting strategies");
                        s.setBrokerClient(this.brokerClient);
                        db.setStrategyStatusAsRunning(s);
                        new Thread() {
                            public void run() {
                                try {
                                    s.start();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
            }

        } catch (SQLException e) {
            e.printStackTrace();
            log.warn("Could not get strategies with status opened from db");
        }
    }

    public void createNewTwoMovingAveragesStrategy(){

        List<Strategy> newStrategies = new ArrayList<>();
        try {
            newStrategies = db.getStrategiesWithStatusOpened();
        } catch (SQLException e) {
            log.error("There are errors accessing the database");
        }

        newStrategies.parallelStream().forEach(s -> {s.setBrokerClient(brokerClient);
            try {
                s.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    /**
     * Listens to incoming messages and convert them into Transaction
     * objects,
     * @param message
     */
    @JmsListener(destination = "executedTransaction.queue")
    public void persistExecutedTransaction(String message){
            Transaction transaction;

            transaction = gson.fromJson(message, Transaction.class);
            log.info("Broker executed and returned transaction " + transaction.toString());
            try {
                db.writeNewTransactionToDB(transaction);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    /*
    Todo: get DB info about volume, stock name, p&l from SQLAccessor
    Todo: have some logic to determine what strategy will be used
    Todo: take that info and put it into the designated strategy
    Todo: the strategy then returns a response if it should buy/sell
    Todo: the return response gets sent to the ActiveMQ in JSON format
    Todo: after the broker fulfills/rejects it sends the response back
      to the StrategyManager who sends it to the SQLAccessor
     */

    //have to check db for p&l and call setter function to update values in strategy?? or is p&l just for frontend?
    //figure out buyprice*volume --> how do you find buyprice in db, what to do if it was never bought
    //have to figure out 10% loss or 1% gain
    //find out if i need to buy sell, buy sell

//    public Double getWhichStrategy() throws IOException {
//        //if bollinger band
//
//    }

}
