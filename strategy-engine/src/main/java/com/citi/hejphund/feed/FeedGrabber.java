package com.citi.hejphund.feed;

import org.apache.http.client.fluent.Request;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.json.*;


public class FeedGrabber {
    private String marketDataBaseURL;
    public FeedGrabber() {
        marketDataBaseURL = "http://nyc31.conygre.com:31";
    }

    public Double getStockPrice(String stockTickerSymbol) throws IOException, JSONException {
        String url = marketDataBaseURL.concat("/Stock/getStockPrice/").concat(stockTickerSymbol);
        String response = getResponse(url);
        JSONObject obj = new JSONObject(response);
        return obj.getDouble("price");
    }

    private String getResponse(String url) throws IOException {
        return Request.Get(url)
                .connectTimeout(1000)
                .socketTimeout(1000)
                .execute().returnContent().asString();
    }

    public Double getStockPriceAtTime(String stockTickerSymbol, LocalTime time) throws IOException, JSONException {
        String url = buildTimeURL(stockTickerSymbol,time);
        String response = getResponse(url);
        JSONObject obj = new JSONObject(response);
        return obj.getDouble("price");
    }

    public List<Double> getStockPriceList(String stockTickerSymbol, Integer count) throws IOException, JSONException {
        String url = marketDataBaseURL.concat("/Stock/getStockPriceList/").concat(stockTickerSymbol).concat("?howManyValues=").concat(Integer.toString(count));
        String response = getResponse(url);

        List<Double> priceList = new ArrayList<>();
        JSONArray arr = new JSONArray(response);
        for (int i = 0; i < arr.length(); i++) {
            JSONObject o = arr.getJSONObject(i);
            Double price = o.getDouble("price");
            priceList.add(price);
        }
        return priceList;
    }

    public Double getStockOpeningPriceAtPeriod(String stockTickerSymbol, Integer period) throws IOException, JSONException {
        String url = getOpenCloseHighLowAtPeriodURL(stockTickerSymbol, period);
        String response = getResponse(url);
        JSONArray arr = new JSONArray(response);
        return arr.getJSONObject(0).getDouble("openingPrice");
    }

    public Double getStockClosingPriceAtPeriod(String stockTickerSymbol, Integer period) throws IOException, JSONException {
        String url = getOpenCloseHighLowAtPeriodURL(stockTickerSymbol, period);
        String response = getResponse(url);
        JSONArray arr = new JSONArray(response);
        return arr.getJSONObject(0).getDouble("closingPrice");
    }

    public Double getStockMaxPriceAtPeriod(String stockTickerSymbol, Integer period) throws IOException, JSONException {
        String url = getOpenCloseHighLowAtPeriodURL(stockTickerSymbol, period);
        String response = getResponse(url);
        JSONArray arr = new JSONArray(response);
        return arr.getJSONObject(0).getDouble("maxPrice");
    }

    public Double getStockMinPriceAtPeriod(String stockTickerSymbol, Integer period) throws IOException, JSONException {
        String url = getOpenCloseHighLowAtPeriodURL(stockTickerSymbol, period);
        String response = getResponse(url);
        JSONArray arr = new JSONArray(response);
        return arr.getJSONObject(0).getDouble("minPrice");
    }

    public List<String> getSymbolList() throws IOException, JSONException {
        String url = marketDataBaseURL
                .concat("/Stock/getSymbolList");
        String response = getResponse(url);

        List<String> symbolList = new ArrayList<>();
        JSONArray symbols = new JSONArray(response);
        for(int i = 0; i < symbols.length();i++) {
            symbolList.add(symbols.getJSONObject(i).getString("companyName"));
        }
        return symbolList;
    }

    public List<String> getSortedSymbolList() throws IOException, JSONException {
        String url = marketDataBaseURL
                .concat("/Stock/getSymbolListOrderedBySymbol");
        String response = getResponse(url);
        List<String> sortedSymbolList = new ArrayList<>();
        JSONArray sortedSymbols = new JSONArray(response);
        for(int i = 0; i < sortedSymbols.length();i++) {
            sortedSymbolList.add(sortedSymbols.getJSONObject(i).getString("companyName"));
        }
        return sortedSymbolList;
    }

    public String getCompanyNameForSymbol(String stockTickerSymbol) throws IOException {
        String url = marketDataBaseURL
                .concat("/Stock/getCompanyNameForSymbol")
                .concat(stockTickerSymbol);
        return getResponse(url);
    }

    private String getOpenCloseHighLowAtPeriodURL(String stockTickerSymbol, Integer period) {
        StringBuilder url = new StringBuilder(marketDataBaseURL);
        url.append("/Stock/getOpenCloseHighLowForSymbolAtPeriod/");
        url.append(stockTickerSymbol);
        url.append("?periodNumber=");
        url.append(period);
        return url.toString();
    }

    private String buildPriceListURL(String stockTickerSymbol, Integer amount) {
        StringBuilder getPriceListURL = new StringBuilder(marketDataBaseURL);
        getPriceListURL.append("/Stock/getStockPriceList/");
        getPriceListURL.append(stockTickerSymbol);
        getPriceListURL.append("?howManyValues=");
        getPriceListURL.append(amount);
        return getPriceListURL.toString();
    }

    public String buildTimeURL(String stockTickerSymbol, LocalTime time){
        StringBuilder getStockPriceAtTimeURL = new StringBuilder(marketDataBaseURL);
        getStockPriceAtTimeURL.append("/Stock/getStockPriceAtTime/");
        getStockPriceAtTimeURL.append(stockTickerSymbol);
        getStockPriceAtTimeURL.append("?time=");
        getStockPriceAtTimeURL.append(time);
        System.out.println(getStockPriceAtTimeURL);
        return getStockPriceAtTimeURL.toString();
    }

//    public void grab() {
//        //System.out.println("HELLO");
//        try {
//            URL url = new URL("http://feed.conygre.com:8080/MockYahoo/quotes.csv?s=goog&f=p0");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod("GET");
//            //System.out.println(url.getQuery());
//            conn.setRequestProperty("Accept", "application/json");
//
//
//            if (conn.getResponseCode() != 200) {
//                throw new RuntimeException("Failed : HTTP error code : "
//                        + conn.getResponseCode());
//            }
//
//            BufferedReader br = new BufferedReader(new InputStreamReader(
//                    (conn.getInputStream())));
//
//            String output;
//            System.out.println("Output from Server .... \n");
//            while ((output = br.readLine()) != null) {
//                System.out.println(output);
//            }
//
//            conn.disconnect();
//
//        } catch (MalformedURLException e) {
//
//            e.printStackTrace();
//
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        }
//    }

}
