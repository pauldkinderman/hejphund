package com.citi.hejphund;


import com.citi.hejphund.feed.TestFeed;
import com.citi.hejphund.strategy.TwoMovingAveragesStrategy;
import com.citi.hejphund.strategy.BollingerBandsStrategy;
import com.citi.hejphund.strategy.Strategy;
import org.junit.Test;

import java.io.IOException;
import java.util.logging.Logger;

public class TestStrategy {

//    public void generalTest(){
//        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
//        ClientService clientService = context.getBean(ClientService.class);
//        Portfolio portfolio = new Portfolio();
//        clientService.assignPortfolio(portfolio);
//
//        clientService.makeOrder(stockID, volume, strategy, duration); //returns order id
//
//        clientService.cancelOrder(orderID);
//
//
//        clientService.getPositions(); // returns a list of all client positions
//
//        OrderService orderService = context.getBean(OrderService.class);
//        Strategy order = clientService.getActiveOrders();
//        Transaction transaction = order.execute(); // returns a transaction if successful else returns
//        client.addPosition(transaction);
//
//        Portfolio clientPortfolio = client.getPortfolio();
//
//        List<Positions> positions = clientPortfolio.getPositions();
//
//        // filter positions to get the newly added position
//        //positions.stream().filter();
//
//    }
    /////////////////////////////////////////
    private static Logger log = Logger.getLogger(String.valueOf(TestFeed.class));

//    @Test
//    public void testForStrategyInput() throws IOException {
//        BollingerBandsStrategy strategy = new BollingerBandsStrategy(3);
//        strategy.makePeriod(1);
//        strategy.makePeriod(2);
//        strategy.makePeriod(3);
//        strategy.makePeriod(4);
//        strategy.makePeriod(5);
//        strategy.makePeriod(6);
//        strategy.makePeriod(7);
//    }

    // irrelevant test name....
    @Test
    public void testGetMSFTStockPriceEvery2Seconds() throws IOException {
        Strategy strategy = new TwoMovingAveragesStrategy(1,80,"Strategy Name 1",
                "AAPL", 200, "OPENED",400);
        try {
            strategy.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
