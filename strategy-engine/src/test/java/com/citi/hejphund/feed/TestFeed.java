package com.citi.hejphund.feed;

import com.citi.hejphund.feed.FeedGrabber;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalTime;
import java.util.logging.Logger;

import static org.junit.Assert.assertTrue;

public class TestFeed {

    private FeedGrabber feedGrabber = new FeedGrabber();
    private static Logger log = Logger.getLogger(String.valueOf(TestFeed.class));

    @Test
    public void testGetStockPriceForMSFT(){
        boolean successful = false;
        try {
            log.info(feedGrabber.getStockPrice("msft").toString());
            successful = true;
        }
        catch(IOException | JSONException e) {
            e.getStackTrace();
        }
        Assert.assertTrue(successful);
    }

    @Test
    public void getStockForMSFTPriceAtMidday(){
        boolean  successful = false;
        try{
            log.info(feedGrabber.getStockPriceAtTime("msft", LocalTime.NOON).toString());
            successful = true;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(successful);
    }

    @Test
    public void getPriceListOf6ForMSFT(){
        boolean successful = false;
        try{
            log.info(feedGrabber.getStockPriceList("msft",6).toString());
            successful = true;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert(successful);
    }

    @Test
    public void getOpeningPriceForFBAtPeriod1(){
        try {
            log.info(feedGrabber.getStockOpeningPriceAtPeriod("fb", 1).toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getClosingPriceForFBAtPeriod1(){
        boolean successful = false;
        try {
            log.info(feedGrabber.getStockClosingPriceAtPeriod("fb",1).toString());
            successful = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        assert(successful);
    }

    @Test
    public void getMaxPriceForFBAtPeriod1(){
        boolean successful = false;
        try {
            log.info(feedGrabber.getStockMaxPriceAtPeriod("fb",1).toString());
            successful = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        assert(successful);
    }

    @Test
    public void getMinPriceForFBAtPeriod1(){
        boolean successful = false;
        try {
            log.info(feedGrabber.getStockMinPriceAtPeriod("fb",1).toString());
            successful = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        assertTrue(successful);
    }

//    @TestStrategy
//    public void testGrab(){
//        feedGrabber.grab();
//    }
}
