package com.citi.hejphund;

import com.citi.hejphund.db.MySQLAccessor;
import com.citi.hejphund.strategy.Strategy;
import com.citi.hejphund.strategy.TwoMovingAveragesStrategy;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class TestStrategies {
    private Logger log = LogManager.getLogger(TwoMovingAveragesStrategy.class);

    private MySQLAccessor mySQLAccessor = new MySQLAccessor();

    @Test
    public void testWeightedAverage() throws SQLException, IOException {
        List<Strategy> openStrategies = mySQLAccessor.getStrategiesWithStatusOpened();
        if(openStrategies.size() > 0){
            Strategy strategy = openStrategies.get(0);
            TwoMovingAveragesStrategy s = null;
            if(strategy instanceof TwoMovingAveragesStrategy) {
                s = (TwoMovingAveragesStrategy) strategy;
            }
            s.getWeightedAverage();
        }
        else{
            log.warn("There are no open strategies!");
        }
    }

    @Test
    public void testStartStrategy() throws SQLException, IOException {
        System.out.println("");
        List<Strategy> openStrategies = mySQLAccessor.getStrategiesWithStatusOpened();
        Strategy s;
        if(openStrategies.size() > 0){
            s = openStrategies.get(0);
            log.info("Starting strategy...");
            s.start();
        }
    }

}
