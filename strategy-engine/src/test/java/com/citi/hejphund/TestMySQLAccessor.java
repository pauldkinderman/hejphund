package com.citi.hejphund;

import com.citi.hejphund.db.MySQLAccessor;
import com.citi.hejphund.strategy.Strategy;
import com.citi.hejphund.strategy.TwoMovingAveragesStrategy;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestMySQLAccessor {
    Logger log = LogManager.getLogger(TestMySQLAccessor.class);
    MySQLAccessor db = new MySQLAccessor();

    @Test
    public void testConnection() throws SQLException, ClassNotFoundException {
        db.establishConnection();
    }

    // this is a really lame test but i have not time to do a more sophisticated one
    @Test
    public void testGetAllStrategiesWithStatusOpened() throws SQLException {
        List<Strategy> newStrategies = db.getStrategiesWithStatusOpened();
        System.out.println(newStrategies.get(0).getStockTickerSymbol());
        assertEquals("Apple MovingAvg1", newStrategies.get(0).getStrategyAlias());
    }

    @Test
    public void testSetStrategyStatusToRunning() throws SQLException {
//        Strategy strategy =
//                new TwoMovingAveragesStrategy(
//                        1,
//                        "Evs trading experiment",
//                        "TWO_MOVING_AVERAGES",
//                        "Citi",
//                        "C",
//                        5,
//                        "OPENED"
//                );
        //BasicConfigurator.configure();
        int numOpenStrategies = db.getStrategiesWithStatusOpened().size();
        log.info("Number of Open Strategies is "+numOpenStrategies);
        db.setStrategyStatusAsRunning(db.getStrategiesWithStatusOpened().get(0));
        log.info("Number of Open Strategies NOW is "+db.getStrategiesWithStatusOpened().size());
        assertEquals(db.getStrategiesWithStatusOpened().size(), numOpenStrategies-1);
    }

    // this test will only pass the first time (!)
    @Test
    public void testWriteNewTransactionToDB() throws SQLException {
        Strategy s = db.getStrategiesWithStatusOpened().get(0);
        Transaction transaction = new Transaction(
                10,
                "FB",
                100.0,
                101.0,
                300,
                s.getTransactionGains(100.0, 101.0),
                LocalDateTime.of(
                        LocalDateTime.now().getYear(),
                        LocalDateTime.now().getMonth(),
                        LocalDateTime.now().getDayOfMonth(),
                        LocalDateTime.now().getHour(),
                        LocalDateTime.now().getMinute(),
                        LocalDateTime.now().getSecond()).toString());
        db.writeNewTransactionToDB(transaction);
    }
}
